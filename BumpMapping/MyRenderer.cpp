#include <IMR/GFX/Auxiliary/Textures.hpp>
#include <IMR/GFX/Auxiliary/Shaders.hpp>

#include "helper/figures.hpp"

#include "MyRenderer.hpp"


using namespace sc::Math;
using namespace IMR::GFX;
using namespace IMR::GFX::API;


const RendererType g_rendererType = 
#ifdef _WIN32
	IG::RendererType::Direct3D9;
#else
	IG::RendererType::OpenGL;
#endif


MyRenderer::MyRenderer(const helper::Window& wnd)
	:	base(g_rendererType, wnd.GetRendererPlaceholder()), 
		_animationSpeed(1 / 3.0f), 
		_camRadius(10)
{ 
	SetRenderState<RenderState::ZEnable>(true);
	SetRenderState<RenderState::ZWriteEnable>(true);
	_shader = Auxiliary::LoadHLShaderFromFiles(*this, 
					"./Shaders/" + g_rendererType.ToString() + "/basic_bump.vsh", 
					"./Shaders/" + g_rendererType.ToString() + "/basic_bump.psh");
	_shader->MakeActive();

	_worldMatrix			= _shader->GetConstant<Matrix_f>("worldMatrix");
	_worldViewProjMatrix	= _shader->GetConstant<Matrix_f>("worldViewProjMatrix");
	_camPos					= _shader->GetConstant<Vector3D_f>("camPos");

	_tex = Auxiliary::LoadTextureFromFile<B8G8R8A8>(*this, "./Textures/normal_map.bmp", TextureSettings(TextureFilter::Linear, TextureFilter::Linear, MipmapFilter::Linear));
	_tex->MakeActive(_shader->GetConstantValue<TextureSampler>("bumpTexture"));

	_mesh = helper::CreateTorus(*this, 3, 1.5f, 128, 512); 
}


void MyRenderer::OnRender()
{
	float t = _timer.Get() * _animationSpeed;	

	_eye.SetPos(Vector3D_f(0, 0, -1) * _camRadius);
	_eye.SetDir(-_eye.GetPos());
	LookAt(_eye);

	ClearBuffers(BufferType::ColorBuffer | BufferType::ZBuffer);
	SetTransform(TransformMode::World, Matrix_f::RotateY(t));

	_camPos->Value = _eye.GetPos();
	_worldMatrix->Value = GetTransform(TransformMode::World);
	_worldViewProjMatrix->Value = GetTransform(TransformMode::World) * 
								  GetTransform(TransformMode::View) * 
								  GetTransform(TransformMode::Projection);

	_mesh->Render();
}
