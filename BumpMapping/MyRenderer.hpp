#pragma once

#include <sc/shared_ptr.hpp>
#include <sc/Timer.hpp>

#include <IMR/GFX/Renderer.hpp>

#include "helper/Window.hpp"


namespace IG = IMR::GFX;


class MyRenderer : public IMR::GFX::Renderer
{
private:
	typedef IG::Renderer							base;
	typedef sc::shared_ptr<IG::IRenderable>			IRenderablePtr;
	typedef sc::shared_ptr<IG::ITexture>			ITexturePtr;
	typedef sc::shared_ptr<IG::HLShader>			HLShaderPtr;
	typedef IG::HLShaderConstant<IG::Matrix_f>		MatrixHLShaderConst;
	typedef IG::HLShaderConstant<IG::Vector3D>		VectorHLShaderConst;
	typedef sc::shared_ptr<MatrixHLShaderConst>		MatrixHLShaderConstPtr;
	typedef sc::shared_ptr<VectorHLShaderConst>		VectorHLShaderConstPtr;


	const float				_animationSpeed;
	const float				_camRadius;

	IG::Eye					_eye;
	IRenderablePtr			_mesh;
	ITexturePtr				_tex;
	HLShaderPtr				_shader;
	MatrixHLShaderConstPtr	_worldMatrix;
	MatrixHLShaderConstPtr	_worldViewProjMatrix;
	VectorHLShaderConstPtr	_camPos;

	sc::Timer				_timer;

public:
	MyRenderer(const helper::Window& wnd);

	virtual void OnRender();
};
