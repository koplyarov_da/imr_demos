#pragma once

#include <sc/shared_ptr.hpp>
#include <sc/Timer.hpp>

#include <IMR/GFX/Renderer.hpp>
#include <IMR/UI/UIManager.hpp>

#include "helper/Window.hpp"


namespace IG = IMR::GFX;
namespace IU = IMR::UI;


class MyRenderer : public IMR::GFX::Renderer
{
private:
	typedef IG::B8G8R8A8							PixelType;
	typedef IG::Renderer							base;
	typedef sc::shared_ptr<IG::Font>				FontPtr;
	typedef sc::shared_ptr<IG::IRenderable>			IRenderablePtr;
	typedef sc::shared_ptr<IG::FrameBuffer>			FrameBufferPtr;
	typedef sc::shared_ptr<IG::Texture2D<PixelType, IG::TextureUsage::RenderTarget> >	RenderTargetTexturePtr;


	const float							_animationSpeed;

	sc::Timer							_timer;

	IG::Eye								_eye;
	FontPtr								_font;
	IRenderablePtr						_mesh;
	IU::UIManager						_uiManager;
	float								_camRadius;
	sc::shared_ptr<IMR::UI::IWidget>	_widget;
	

public:
	MyRenderer(const helper::Window& wnd);

	IU::UIManager& GetUIManager()	{ return _uiManager; }

	virtual void OnRender();
};
