#ifndef __IMR_UI_CODEGEN_BasicWidget_H__
#define __IMR_UI_CODEGEN_BasicWidget_H__


#include <IMR/UI/Widgets/Button.hpp>
#include <IMR/UI/Widgets/Canvas.hpp>
#include <IMR/UI/Widgets/Label.hpp>
#include <IMR/UI/Widgets/ProgressBar.hpp>
#include <IMR/UI/Widgets/Slider.hpp>
#include <IMR/UI/Widgets/TabControl.hpp>
#include <IMR/UI/Widgets/TabPage.hpp>
#include <IMR/UI/Widgets/TextBox.hpp>


class BasicWidget;


class BasicWidget_Designer
{
protected: 
	sc::shared_ptr<IMR::UI::Label> _camRadiusLabel;
	sc::shared_ptr<IMR::UI::Slider> _camRadiusSlider;
	sc::shared_ptr<IMR::UI::Button> _cancelButton;
	sc::shared_ptr<IMR::UI::Canvas> _canvas;
	sc::shared_ptr<IMR::UI::Label> _noContentLabel;
	sc::shared_ptr<IMR::UI::Button> _okButton;
	sc::shared_ptr<IMR::UI::ProgressBar> _progressBar;
	sc::shared_ptr<IMR::UI::TabControl> _tabControl;
	sc::shared_ptr<IMR::UI::TabPage> _tabPage1;
	sc::shared_ptr<IMR::UI::TabPage> _tabPage2;
	sc::shared_ptr<IMR::UI::TextBox> _textBox;

protected: 
	void InitializeWidget(BasicWidget* instance);
	virtual ~BasicWidget_Designer() { }
};


#endif
