#include <IMR/GFX/Auxiliary/Textures.hpp>
#include <IMR/GFX/Auxiliary/Shaders.hpp>
#include <IMR/UI/Styles/Basic.hpp>
#include <IMR/UI/Resources/NamedResourceManager.hpp>

#include "MyRenderer.hpp"
#include "helper/figures.hpp"
#include "BasicWidget.hpp"


using namespace sc::Math;
using namespace IMR::GFX;
using namespace IMR::GFX::API;


const RendererType g_rendererType = 
#ifdef _WIN32
	IG::RendererType::Direct3D9;
#else
	IG::RendererType::OpenGL;
#endif


struct ImageResGetter : public IMR::UI::NamedResourceResolver<IMR::UI::ImageResourceData>
{
	virtual IMR::UI::ImageResourceData* GetResource(const std::string&)
	{
		std::vector<IMR::UI::RGBA> pixels(128 * 128);

		float k = 255.f/127;

		for (int j = 0; j < 128; ++j)
			for (int i = 0; i < 128; ++i)
				pixels[i + j * 128] = 
						IMR::UI::RGBA((unsigned char)(i * k), 
									  (unsigned char)(j * k), 
									  (unsigned char)((127 - i) * k), 
									  /*unsigned char((127 - j) * k)*/255);

		return new IMR::UI::ImageResourceData(128, 128, pixels);
	}
};


MyRenderer::MyRenderer(const helper::Window& wnd)
	:	base(g_rendererType, wnd.GetRendererPlaceholder()), 
		_animationSpeed(1 / 3.0f), 
		_camRadius(10)
{ 
	using namespace IMR::UI;

	NamedResourceManager::RegisterImageResourceResolver(new ImageResGetter);

	EnableLight(0, true);
	SetLight(0, DirectionalLight(Vector3D_f(-0.1f, -0.5f, -0.2f).Normalize()));
	_font = UseSystemFont(wnd.GetFont());
	_mesh = helper::CreateTorus(*this, 3, 1.5f, 32, 128); 
	
	_uiManager.AddWidget(_widget = new BasicWidget(_uiManager, _camRadius));
	_uiManager.SetStyle(new Styles::Basic(*this, _font, Styles::Basic::ColorScheme::YellowBlue));
}


void MyRenderer::OnRender()
{
	float t = _timer.Get() * _animationSpeed;	

	ClearBuffers(BufferType::ColorBuffer | BufferType::ZBuffer);

	SetRenderState<RenderState::Lighting>(true);
	SetRenderState<RenderState::ZEnable>(true);
	SetRenderState<RenderState::ZWriteEnable>(true);

	_eye.SetPos(Vector3D_f(0, 0, 1) * _camRadius);
	_eye.SetDir(-_eye.GetPos());
	LookAt(_eye);
	SetTransform(TransformMode::World, Matrix_f::RotateY(t));

	_mesh->Render();
	_uiManager.RenderWidgets();
	_uiManager.Process();
}
