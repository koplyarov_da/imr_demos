#include "BasicWidget_Designer.hpp"
#include "BasicWidget.hpp"

#include <IMR/UI/Layout/AnchorLayoutManager.hpp>
#include <IMR/UI/Rect.hpp>
#include <IMR/UI/Widgets/Window.hpp>
#include <sc/shared_ptr.hpp>



void BasicWidget_Designer::InitializeWidget(BasicWidget* instance)
{
	// BasicWidget properties
	instance->SetBehavior(IMR::UI::WindowBehavior::Resizable);
	instance->SetLayoutManager(new IMR::UI::AnchorLayoutManager());
	instance->SetMinSize(IMR::UI::Size(270, 195));
	instance->SetPosition(IMR::UI::Point(20, 20));
	instance->SetSize(IMR::UI::Size(360, 295));
	instance->SetText(L"English text | Russian text");

	_camRadiusLabel = new IMR::UI::Label;
	// _camRadiusLabel properties
	_camRadiusLabel->SetPosition(IMR::UI::Point(10, 10));
	_camRadiusLabel->SetSize(IMR::UI::Size(100, 30));
	_camRadiusLabel->SetText(L"Camera distance:");

	_camRadiusSlider = new IMR::UI::Slider;
	// _camRadiusSlider properties
	_camRadiusSlider->SetMaxValue(50);
	_camRadiusSlider->SetMinValue(5);
	_camRadiusSlider->SetPosition(IMR::UI::Point(130, 10));
	_camRadiusSlider->SetSize(IMR::UI::Size(200, 30));
	_camRadiusSlider->SetValue(10);
	// _camRadiusSlider events
	_camRadiusSlider->ValueChanged.connect(IMR::UI::BindThisToEventHandlerProc(instance, &BasicWidget::_sliderOnValueChanged));

	_cancelButton = new IMR::UI::Button;
	// _cancelButton properties
	_cancelButton->SetPosition(IMR::UI::Point(277, 225));
	_cancelButton->SetSize(IMR::UI::Size(70, 30));
	_cancelButton->SetText(L"Cancel");
	_cancelButton->SetVisible(true);
	// _cancelButton events
	_cancelButton->Click.connect(IMR::UI::BindThisToEventHandlerProc(instance, &BasicWidget::_cancelButton_OnClick));

	_canvas = new IMR::UI::Canvas;
	// _canvas properties
	_canvas->SetPosition(IMR::UI::Point(0, 0));
	_canvas->SetSize(IMR::UI::Size(340, 205));
	_canvas->SetViewport(IMR::UI::Rect(IMR::UI::Point(0, 0), IMR::UI::Point(1, 1)));

	_noContentLabel = new IMR::UI::Label;
	// _noContentLabel properties
	_noContentLabel->SetPosition(IMR::UI::Point(120, 87));
	_noContentLabel->SetSize(IMR::UI::Size(100, 30));
	_noContentLabel->SetText(L"No content");
	// _noContentLabel events
	_noContentLabel->MouseDoubleClick.connect(IMR::UI::BindThisToEventHandlerProc(instance, &BasicWidget::_okButton_OnClick));

	_okButton = new IMR::UI::Button;
	// _okButton properties
	_okButton->SetPosition(IMR::UI::Point(200, 225));
	_okButton->SetSize(IMR::UI::Size(67, 30));
	_okButton->SetText(L"OK");
	// _okButton events
	_okButton->Click.connect(IMR::UI::BindThisToEventHandlerProc(instance, &BasicWidget::_okButton_OnClick));

	_progressBar = new IMR::UI::ProgressBar;
	// _progressBar properties
	_progressBar->SetMaxValue(50);
	_progressBar->SetMinValue(5);
	_progressBar->SetPosition(IMR::UI::Point(7, 230));
	_progressBar->SetSize(IMR::UI::Size(180, 20));
	_progressBar->SetValue(10);

	_tabControl = new IMR::UI::TabControl;
	// _tabControl properties
	_tabControl->SetPosition(IMR::UI::Point(7, 10));
	_tabControl->SetSize(IMR::UI::Size(340, 205));

	_tabPage1 = new IMR::UI::TabPage;
	// _tabPage1 properties
	_tabPage1->SetLayoutManager(new IMR::UI::AnchorLayoutManager());
	_tabPage1->SetText(L"Tab with controls");

	_tabPage2 = new IMR::UI::TabPage;
	// _tabPage2 properties
	_tabPage2->SetLayoutManager(new IMR::UI::AnchorLayoutManager());
	_tabPage2->SetText(L"Empty tab");

	_textBox = new IMR::UI::TextBox;
	// _textBox properties
	_textBox->SetPosition(IMR::UI::Point(10, 45));
	_textBox->SetSize(IMR::UI::Size(320, 150));
	_textBox->SetText(L"Some stuff");

	// Adding children
	instance->AddChild(_tabControl, IMR::UI::AnchorLayoutConstraints(IMR::UI::Anchor::Top | IMR::UI::Anchor::Left | IMR::UI::Anchor::Right | IMR::UI::Anchor::Bottom));
	instance->AddChild(_progressBar, IMR::UI::AnchorLayoutConstraints(IMR::UI::Anchor::Left | IMR::UI::Anchor::Right | IMR::UI::Anchor::Bottom));
	instance->AddChild(_okButton, IMR::UI::AnchorLayoutConstraints(IMR::UI::Anchor::Right | IMR::UI::Anchor::Bottom));
	instance->AddChild(_cancelButton, IMR::UI::AnchorLayoutConstraints(IMR::UI::Anchor::Right | IMR::UI::Anchor::Bottom));
	_tabControl->AddTabPage(_tabPage1);
	_tabControl->AddTabPage(_tabPage2);
	_tabPage1->AddChild(_camRadiusLabel, IMR::UI::AnchorLayoutConstraints(IMR::UI::Anchor::Top | IMR::UI::Anchor::Left));
	_tabPage1->AddChild(_camRadiusSlider, IMR::UI::AnchorLayoutConstraints(IMR::UI::Anchor::Top | IMR::UI::Anchor::Left | IMR::UI::Anchor::Right));
	_tabPage1->AddChild(_textBox, IMR::UI::AnchorLayoutConstraints(IMR::UI::Anchor::Top | IMR::UI::Anchor::Left | IMR::UI::Anchor::Right | IMR::UI::Anchor::Bottom));
	_tabPage2->AddChild(_canvas, IMR::UI::AnchorLayoutConstraints(IMR::UI::Anchor::Top | IMR::UI::Anchor::Left | IMR::UI::Anchor::Right | IMR::UI::Anchor::Bottom));
	_tabPage2->AddChild(_noContentLabel, IMR::UI::AnchorLayoutConstraints(IMR::UI::Anchor::None));
}
