#include "helper/RenderWindow.hpp"


template < typename RendererType >
class MyWindow : public helper::RenderWindow<RendererType>
{
	virtual void OnMouseMove(int x, int y, IMR::UI::MouseButton button)
	{ SendMouseControllerAction(x, y, button, IMR::UI::MouseEvent::Move); }

	virtual void OnMouseUp(int x, int y, IMR::UI::MouseButton button)
	{ SendMouseControllerAction(x, y, button, IMR::UI::MouseEvent::ButtonUp); }

	virtual void OnMouseDown(int x, int y, IMR::UI::MouseButton button)
	{ SendMouseControllerAction(x, y, button, IMR::UI::MouseEvent::ButtonDown); }


	void SendMouseControllerAction(int x, int y, IMR::UI::MouseButton button, IMR::UI::MouseEvent event)
	{
		helper::RenderWindow<RendererType>::GetRenderer().GetUIManager().React(new IMR::UI::MouseControllerAction((float)x, (float)y, 0, event, button));
	}
};
