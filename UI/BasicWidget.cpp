#include "BasicWidget.hpp"

using namespace IMR::UI;


BasicWidget::BasicWidget(const IMR::UI::UIManager& uiManager, float& camRadius)
	: _uiManager(uiManager), _style(false), _camRadius(camRadius)
{ 
	BasicWidget_Designer::InitializeWidget(this); 

	std::vector<Point> points;

	points.push_back(Point(0, 0));
	points.push_back(Point(0.7f, 0.3f));
	points.push_back(Point(1, 1));

	_canvas->AddPolygon(points, RGBA(0, 255, 0), RGBA(255, 0, 0));
}

