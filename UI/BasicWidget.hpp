#ifndef __BASICWIDGET_HPP__
#define __BASICWIDGET_HPP__


#include <sc/exceptions.hpp>

#include <IMR/GFX/Renderer.hpp>

#include <IMR/UI/Widgets/Window.hpp>
#include <IMR/UI/Controller/MouseControllerAction.hpp>
#include <IMR/UI/Styles/Basic.hpp>
#include <IMR/UI/UIManager.hpp>

#include "BasicWidget_Designer.hpp"


class BasicWidget : 
	private BasicWidget_Designer, 
	public IMR::UI::Window 
{
	friend class BasicWidget_Designer;

	const IMR::UI::UIManager&	_uiManager;
	float&						_camRadius;
	bool						_style;

public:
	BasicWidget(const IMR::UI::UIManager& uiManager, float& camRadius);

private:
	void _okButton_OnClick(const IMR::UI::IEvent& e) 
	{ 
		using namespace IMR::UI;
		Styles::Basic& basic_style = dynamic_cast<Styles::Basic&>(_uiManager.GetStyle());
		if (!_style)
			basic_style.SetColorScheme(Styles::Basic::ColorScheme::Default);
		else
			basic_style.SetColorScheme(Styles::Basic::ColorScheme::YellowBlue);
		_style = !_style;
	}

	void _cancelButton_OnClick(const IMR::UI::ControllerActionEvent& e) 
	{ SC_THROW(std::runtime_error("Cancel clicked, stopping demo.")); }

	void _sliderOnValueChanged(const IMR::UI::IEvent& e)
	{ _camRadius = (float)_camRadiusSlider->GetValue(); _progressBar->SetValue(_camRadiusSlider->GetValue()); }
};


#endif
