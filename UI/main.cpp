#include "helper/helper.hpp"
#include "MyRenderer.hpp"
#include "MyWindow.hpp"


#ifdef _WIN32
int WINAPI WinMain(HINSTANCE /*hInstance*/, HINSTANCE /*hPrevInstance*/, LPSTR /*lpCmdLine*/, int /*nCmdShow*/)
#else
int main(int argc, char* argv[])
#endif
{
#ifdef _DARWIN
	std::string exec_name = argv[0];
	size_t i = exec_name.find_last_of('/');
	std::string exec_path;
	std::copy(exec_name.begin(), exec_name.begin() + i, std::back_inserter(exec_path));
	std::cout << "Changing dir to '" << exec_path << "'" << std::endl;
	chdir(exec_path.c_str());
#endif
	return helper::SampleRendererWindowLifeCycle<MyWindow<MyRenderer> >();
}
