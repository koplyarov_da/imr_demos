#include <stdlib.h>

#include <IMR/GFX/Auxiliary/Textures.hpp>
#include <IMR/GFX/Auxiliary/Shaders.hpp>

#include <iostream>

#include "helper/figures.hpp"

#include "MyRenderer.hpp"


using namespace sc::Math;
using namespace IMR::GFX;
using namespace IMR::GFX::API;


const RendererType g_rendererType = IG::RendererType::OpenGL;


MyRenderer::MyRenderer(const helper::Window& wnd)
	:	base(g_rendererType, wnd.GetRendererPlaceholder()), 
		_animationSpeed(1 / 10.0f), 
		_camRadius(1000)
{ 
	SetRenderState<RenderState::ZEnable>(true);
	SetRenderState<RenderState::ZWriteEnable>(true);
	SetRenderState<RenderState::Lighting>(true);

	EnableLight(0, true);
	SetLight(0, DirectionalLight(Vector3D_f(-0.1f, -0.5f, -0.2f).Normalize()));

	GetSceneInfo().SetZFar(10000);

	_mesh = CreateLandscape("./Heightmaps/basic_landscape.bmp", "./Palettes/height_pal.bmp"); 
}


MyRenderer::IRenderablePtr MyRenderer::CreateLandscape(const std::string& heightMapFilename, const std::string& paletteFilename)
{
	typedef VertexNormalDiffuse				VertexType;
	typedef Auxiliary::Image<Luminance16>	ImageType;

	struct Helper
	{
		const ImageType& _image;

		Helper(const ImageType& image) : _image(image) { }

		Vector3D_f ToVertex(int i, int j) const
		{
			if (i < 0) i = 0;
			if (j < 0) j = 0;
			if (i >= _image.GetWidth()) i = _image.GetWidth() - 1;
			if (j >= _image.GetHeight()) j = _image.GetHeight() - 1;

			return Vector3D_f((float)i, _image.GetData()[i + j * _image.GetWidth()].Luminance * 0.001f, (float)j) * 10;
		}
	};

	Auxiliary::Image<B8G8R8A8>		palette(paletteFilename);
	Auxiliary::Image<Luminance16>	height_map(heightMapFilename);
	
	Helper hlp(height_map);

	std::vector<unsigned>	indices;
	std::vector<VertexType>	vertices(height_map.GetWidth() * height_map.GetHeight());

	for (int j = 0; j < height_map.GetHeight(); ++j)
		for (int i = 0; i < height_map.GetWidth(); ++i)
		{
			float h = (float)height_map.GetData()[i + j * height_map.GetWidth()].Luminance / 0xFFFF;
			float k = (rand() % 32768) / 32767.f;

			B8G8R8A8 color = palette.GetData()[int(h * (palette.GetWidth() - 1)) + palette.GetWidth() * int(k * (palette.GetHeight() - 1))];

			if (g_rendererType == IG::RendererType::OpenGL)
				sc::Swap(color.R, color.B);

			Vector3D_f n1 = -Vector3D_f::CrossProduct(hlp.ToVertex(i-1, j) - hlp.ToVertex(i+1, j), 
													  hlp.ToVertex(i, j-1) - hlp.ToVertex(i, j+1)).Normalize();
			Vector3D_f n2 = -Vector3D_f::CrossProduct(hlp.ToVertex(i-1, j-1) - hlp.ToVertex(i+1, j+1), 
													  hlp.ToVertex(i+1, j-1) - hlp.ToVertex(i-1, j+1)).Normalize();

			vertices[i + j * height_map.GetWidth()] = 
				VertexType(hlp.ToVertex(i, j) - Vector3D_f(height_map.GetWidth() / 2.f, 0, height_map.GetHeight() / 2.f) * 10, 
						   (n1 + n2).Normalize(), color);
		}

	for (int j = 0; j < height_map.GetHeight() - 1; ++j)
		for (int i = 0; i < height_map.GetWidth() - 1; ++i)
		{
			indices.push_back(i + j * height_map.GetWidth());
			indices.push_back(i + (j + 1) * height_map.GetWidth());
			indices.push_back(i + 1 + j * height_map.GetWidth());
			indices.push_back(i + (j + 1) * height_map.GetWidth());
			indices.push_back(i + 1 + (j + 1) * height_map.GetWidth());
			indices.push_back(i + 1 + j * height_map.GetWidth());
		}

	sc::shared_ptr<IndexArray<unsigned> > ia = CreateIndexArray<unsigned>();
	sc::shared_ptr<VertexArray<VertexType> > va = CreateVertexArray<VertexType>(DrawMode::Triangles);
	va->SetData(vertices);
	ia->SetData(indices);

	return va->GetRenderable(ia);
}


void MyRenderer::OnRender()
{
	float t = _timer.Get() * _animationSpeed;	

	_eye.SetPos(Vector3D_f(2 * Cos(t), 3, 2 * Sin(t)) * _camRadius);
	_eye.SetDir(-_eye.GetPos());
	//_eye.SetDir(Vector3D_f(Cos(t), -0.3, -Sin(t)) - Vector3D_f(Sin(t), 0, Cos(t)) * 0.5);
	//_eye.SetPos(Vector3D_f(0, 0, -1) * _camRadius);
	LookAt(_eye);

	ClearBuffers(BufferType::ColorBuffer | BufferType::ZBuffer);
	SetTransform(TransformMode::World, Matrix_f::Translate(0, -50, 0));

	_mesh->Render();
}
