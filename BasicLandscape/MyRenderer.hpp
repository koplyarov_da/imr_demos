#pragma once

#include <sc/shared_ptr.hpp>
#include <sc/Timer.hpp>

#include <IMR/GFX/Renderer.hpp>

#include "helper/Window.hpp"


namespace IG = IMR::GFX;


class MyRenderer : public IMR::GFX::Renderer
{
private:
	typedef IG::Renderer							base;
	typedef sc::shared_ptr<IG::IRenderable>			IRenderablePtr;

	const float				_animationSpeed;
	const float				_camRadius;

	IG::Eye					_eye;
	IRenderablePtr			_mesh;

	sc::Timer				_timer;

public:
	MyRenderer(const helper::Window& wnd);

	virtual void OnRender();

private:
	IRenderablePtr CreateLandscape(const std::string& heightMapFilename, const std::string& paletteFilename);
};
