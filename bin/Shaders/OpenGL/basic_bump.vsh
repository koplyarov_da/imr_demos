#version 120

uniform mat4x4 	worldMatrix;
uniform mat4x4 	worldViewProjMatrix;
uniform vec3	camPos;


void main()
{
	//vec4 pos 		= ftransform();//gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
	vec4 pos 		= worldViewProjMatrix * gl_Vertex;

	gl_Position		= pos;
	gl_TexCoord[0]	= gl_MultiTexCoord0;

	gl_TexCoord[1]	= worldMatrix * vec4(gl_Normal, 0);
	gl_TexCoord[2]	= worldMatrix  * gl_MultiTexCoord1;
	gl_TexCoord[3]	= vec4(camPos, 1) - pos; 
}
