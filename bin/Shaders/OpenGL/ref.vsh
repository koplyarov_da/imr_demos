varying vec3 ct;
varying vec3 ctl;
varying vec3 _t;
varying vec3 _b;
varying vec3 _n;
varying vec3 ht;


uniform vec3 eyePos;
uniform mat4x4	world;
uniform mat4x4	normalMatrix;
uniform mat4x4	worldViewProj;



void main(void) {

	vec3	p = vec3(world * gl_Vertex);
	_n = normalize(mat3x3(normalMatrix) * gl_Normal);
	_t = normalize(mat3x3(normalMatrix) * gl_MultiTexCoord1.xyz);
	_b = normalize(cross(_n, _t));
	ct = -normalize(vec3(eyePos) - p);

	mat3 T = transpose(mat3(_t, _b, _n));
	ht = T * normalize(normalize(vec3(gl_LightSource[1].position) - p) + ct);
	ctl = T * ct;

	gl_Position     = worldViewProj * gl_Vertex;
	gl_TexCoord[0] = gl_MultiTexCoord0;
}

