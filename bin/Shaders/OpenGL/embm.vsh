varying vec3 ct;
varying vec3 _t;
varying vec3 _b;
varying vec3 _n;


uniform vec3	eyePos;
uniform mat4	normalMatrix;
uniform mat4	world;
uniform mat4	worldViewProj;



void main() 
{

	_n = normalize((normalMatrix * vec4(gl_Normal, 0)).xyz);
	_t = normalize((normalMatrix * vec4(gl_MultiTexCoord1.xyz, 0)).xyz);
	_b = normalize(cross(_n, _t));

	ct = normalize(-vec3(eyePos) + vec3(world * gl_Vertex));

	gl_Position		= worldViewProj * gl_Vertex;
	gl_TexCoord[0]	= gl_MultiTexCoord0;
}

