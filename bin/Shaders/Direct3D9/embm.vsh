struct VS_OUTPUT 
{
   float4 pos:		POSITION;
   float2 texCoord:	TEXCOORD0;
   float3 ct:		TEXCOORD1;
   float3 _t:		TEXCOORD2;
   float3 _b:		TEXCOORD3;
   float3 _n:		TEXCOORD4;
};


struct VS_INPUT
{
   float4 pos:		POSITION;
   float3 normal:	NORMAL;
   float2 texCoord:	TEXCOORD0;
   float3 tangent:	TEXCOORD1;
};


float4x4 normalMatrix;
float4x4 world;
float4x4 worldViewProj;
float3 eyePos;



VS_OUTPUT main(VS_INPUT inp) 
{
	VS_OUTPUT outp;

	outp._n = normalize(mul(inp.normal, normalMatrix));
	outp._t = normalize(mul(inp.tangent.xyz, normalMatrix));
	outp._b = normalize(cross(outp._n, outp._t));

	outp.ct = -normalize(eyePos - mul(inp.pos, world).xyz);

	outp.pos      = mul(inp.pos, worldViewProj);
	outp.texCoord = inp.texCoord;

	return outp;
}

