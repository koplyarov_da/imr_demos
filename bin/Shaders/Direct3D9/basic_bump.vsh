struct VS_OUTPUT 
{
   float4 pos:		POSITION;
   float2 texCoord:	TEXCOORD0;
   float3 normal:	TEXCOORD1;
   float3 tangent:	TEXCOORD2;
   float3 camVec:	TEXCOORD3;
};


struct VS_INPUT
{
   float4 pos:		POSITION;
   float3 normal:	NORMAL;
   float2 texCoord:	TEXCOORD0;
   float3 tangent:	TEXCOORD1;
};



float4x4 	worldMatrix;
float4x4 	worldViewProjMatrix;
float3		camPos;


VS_OUTPUT main(VS_INPUT inp)
{
	VS_OUTPUT o;

	o.pos		= mul(inp.pos, worldViewProjMatrix);
	o.texCoord	= inp.texCoord;

	o.normal	= mul(inp.normal, worldMatrix);
	o.tangent	= mul(inp.tangent, worldMatrix);
	o.camVec	= camPos - o.pos; 
	
	return o;
}