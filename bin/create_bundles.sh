#!/bin/sh
##############################################

Try()
{
	$@
	if [ ! $? -eq 0 ]; then
		echo The execution of \'$@\' failed!
		exit 1
	fi
}


CreateInfoPList()
{
	Try echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
	Try echo "<!DOCTYPE plist PUBLIC \"-//Apple Computer//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">"
	Try echo "<plist version=\"1.0\">"
	Try echo "<dict>"
	Try echo "	<key>CFBundleDevelopmentRegion</key>"
	Try echo "	<string>English</string>"
	Try echo "	<key>CFBundleExecutable</key>"
	Try echo "	<string>$1</string>"
	Try echo "	<key>CFBundleGetInfoString</key>"
	Try echo "	<string>1.0</string>"
	Try echo "	<key>CFBundleIconFile</key>"
	Try echo "	<string>none.icns</string>"
	Try echo "	<key>CFBundleIdentifier</key>"
	Try echo "	<string>com.imr.gfx</string>"
	Try echo "	<key>CFBundleInfoDictionaryVersion</key>"
	Try echo "	<string>6.0</string>"
	Try echo "	<key>CFBundleName</key>"
	Try echo "	<string>IMR_GFX_$1</string>"
	Try echo "	<key>CFBundlePackageType</key>"
	Try echo "	<string>APPL</string>"
	Try echo "	<key>CFBundleShortVersionString</key>"
	Try echo "	<string>1.0</string>"
	Try echo "	<key>CFBundleSignature</key>"
	Try echo "	<string>IMR_GFX_$1</string>"
	Try echo "	<key>CFBundleURLTypes</key>"
	Try echo "	<array>"
	Try echo "		<dict>"
	Try echo "			<key>CFBundleURLIconFile</key>"
	Try echo "			<string>none.icns</string>"
	Try echo "			<key>CFBundleURLName</key>"
	Try echo "			<string>http://www.imr_gfx.com</string>"
	Try echo "			<key>CFBundleURLSchemes</key>"
	Try echo "			<array>"
	Try echo "				<string>imr_gfx</string>"
	Try echo "			</array>"
	Try echo "		</dict>"
	Try echo "	</array>"
	Try echo "	<key>CFBundleVersion</key>"
	Try echo "	<string>1.0</string>"
	Try echo "</dict>"
	Try echo "</plist>"
}


CreateBundle()
{
	echo Creating \'./$1.app\' bundle...

	if [ -e ./$1.app ]; then 
		Try rm -rf ./$1.app
	fi

	Try mkdir ./$1.app
	Try mkdir ./$1.app/Contents
	Try mkdir ./$1.app/Contents/MacOS

	Try ln -s ../../../$1 ./$1.app/Contents/MacOS/$1
	Try ln -s ../../../Cubemaps ./$1.app/Contents/MacOS/Cubemaps
	Try ln -s ../../../Heightmaps ./$1.app/Contents/MacOS/Heightmaps
	Try ln -s ../../../Palettes ./$1.app/Contents/MacOS/Palettes
	Try ln -s ../../../Shaders ./$1.app/Contents/MacOS/Shaders
	Try ln -s ../../../Textures ./$1.app/Contents/MacOS/Textures
	Try CreateInfoPList $1 > ./$1.app/Contents/Info.plist

	echo Done.
}

##############################################

CreateBundle BasicLandscape
CreateBundle BumpMapping
CreateBundle EMBM
CreateBundle UI
