#ifndef __ICODEGENERATOR_HPP__
#define __ICODEGENERATOR_HPP__


#include <vector>

#include <sc/shared_ptr.hpp>

#include "GeneratedCode.hpp"


struct ICodeGenerator
{
	virtual ~ICodeGenerator() { }

	virtual std::vector<sc::shared_ptr<GeneratedCode> > Generate() = 0;
};


#endif