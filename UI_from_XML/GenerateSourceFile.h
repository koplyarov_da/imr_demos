#ifndef __GENERATESOURCEFILE_H__
#define __GENERATESOURCEFILE_H__


#include <ctype.h>
#include <iostream>
#include <string>

#include <sc/XML/Reader.hpp>
#include <sc/exceptions.hpp>

#include <IMR/UI/Beans/BeansManager.hpp>

#include "Utils.h"


void InitMembersAndProperties(const std::string& className, const std::string& variableName, sc::shared_ptr<sc::XML::Element> node, std::ostream& outStream)
{
	const IMR::UI::Bean& bean = IMR::UI::BeansManager::GetBean(ToAnsiString(node->GetName()));

	std::string entity_name = ToAnsiString(node->GetAttribute(L"name"));

	sc::XML::Element::AttributeKeysCollection::const_iterator attr_it;
	for (attr_it = node->GetAttributeNames().begin(); attr_it != node->GetAttributeNames().end(); ++attr_it)
	{
		if (*attr_it == L"name" || *attr_it == L"layoutConstraints")
			continue;

		std::string cpp_prop_name = ToAnsiString(*attr_it);
		cpp_prop_name[0] = toupper(cpp_prop_name[0]);

		sc::shared_ptr<IMR::UI::IBeanPropertyValue> bean_prop_val =
			bean.GetPropertyDescriptor(ToAnsiString(*attr_it))->GetType()->CreateInstance();

		bean_prop_val->DeserializeFromXMLAttribute(node->GetAttribute(*attr_it));

		std::string bean_prop_val_cpp;
		if (sc::InstanceOf<IMR::UI::IEventBeanPropertyValue>(*bean_prop_val))
			bean_prop_val_cpp = "IMR::UI::BindEventHandlerProcThisBinder(instance, &" + className + "::" + bean_prop_val->SerializeToCppCode() + ")";
		else
			bean_prop_val_cpp = bean_prop_val->SerializeToCppCode();

		outStream << "\t" << variableName << "->Set" << cpp_prop_name << "(" << bean_prop_val_cpp << ");" << std::endl;
	}
	outStream << std::endl;

	sc::XML::Element::ChildrenCollection::const_iterator children_it;
	for (children_it = node->GetChildren().begin(); children_it != node->GetChildren().end(); ++children_it)
	{
		const IMR::UI::Bean& child_bean = IMR::UI::BeansManager::GetBean(ToAnsiString((*children_it)->GetName()));
		std::string child_name = ToAnsiString((*children_it)->GetAttribute(L"name"));
		outStream << "\t//" << child_name << " initialization" << std::endl;
		outStream << "\t" << child_name << " = new " << child_bean.GetCppTypeName() << ";" << std::endl;
		InitMembersAndProperties(className, child_name, *children_it, outStream);
	}
}


void AddChildren(const std::string& variableName, sc::shared_ptr<sc::XML::Element> node, std::ostream& outStream)
{
	sc::XML::Element::ChildrenCollection::const_iterator children_it;
	for (children_it = node->GetChildren().begin(); children_it != node->GetChildren().end(); ++children_it)
	{
		const IMR::UI::Bean& child_bean = IMR::UI::BeansManager::GetBean(ToAnsiString((*children_it)->GetName()));
		std::string child_name = ToAnsiString((*children_it)->GetAttribute(L"name"));
		std::string layout_constraints;
		if ((*children_it)->HasAttribute(L"layoutConstraints"))
		{
			std::string layout_constraints_val = ToAnsiString((*children_it)->GetAttribute(L"layoutConstraints"));
			size_t eq_pos = layout_constraints_val.find_first_of('=');
			if (eq_pos == std::string::npos || eq_pos == 0 || eq_pos == layout_constraints_val.size() - 1)
				SC_THROW(std::runtime_error("Invalid layoutConstraints value!"));
			if (std::string(layout_constraints_val, 0, eq_pos) != "anchor")
				SC_THROW(std::runtime_error("Unknown layoutConstraints type!"));
			std::string anchors(layout_constraints_val, eq_pos + 1);
			size_t delim_pos = 0;
			layout_constraints = ", IMR::UI::AnchorLayoutConstraints(";
			bool more_than_one = false;
			while (delim_pos != std::string::npos)
			{
				if (delim_pos == anchors.size() - 1)
					SC_THROW(std::runtime_error("Invalid layoutConstraints value!"));

				if (more_than_one)
					layout_constraints += " | ";

				size_t next_delim_pos = anchors.find_first_of(',', delim_pos + more_than_one);
				std::string the_anchor;
				if (next_delim_pos == std::string::npos)
					the_anchor = std::string(anchors, delim_pos + more_than_one);
				else
					the_anchor = std::string(anchors, delim_pos + more_than_one, (next_delim_pos - delim_pos - more_than_one));
				if (the_anchor.empty())
					SC_THROW(std::runtime_error("Invalid layoutConstraints value!"));
				the_anchor[0] = toupper(the_anchor[0]);
				layout_constraints += "IMR::UI::Anchor::" + the_anchor;

				more_than_one = true;
				delim_pos = next_delim_pos;
			}
			layout_constraints += ")";
		}
		outStream << "\t" << variableName << "->AddChild(" << child_name << layout_constraints << ");" << std::endl;
		AddChildren(child_name, *children_it, outStream);
	}
}


void GenerateSourceFile(sc::shared_ptr<sc::XML::Element> node, const std::string& headerFileName, std::ostream& outStream)
{
	const IMR::UI::Bean& bean = IMR::UI::BeansManager::GetBean(ToAnsiString(node->GetName()));

	std::string entity_name = ToAnsiString(node->GetAttribute(L"name"));

	outStream << "#include \"" << headerFileName << "\"" << std::endl;
	outStream << "#include \"" << entity_name << ".hpp\"" << std::endl;
	outStream << std::endl;
	outStream << "#include <IMR/UI/Layout/AnchorLayoutManager.hpp>" << std::endl;
	outStream << std::endl;
	outStream << "//#include <whatever else>" << std::endl;
	outStream << std::endl << std::endl << std::endl;

	outStream << "void " << entity_name << "_Designer::InitializeWidget(" << entity_name /*bean.GetCppTypeName()*/ << "* instance)" << std::endl;
	outStream << "{" << std::endl;

	outStream << "\t//" << entity_name << " properties" << std::endl;
	InitMembersAndProperties(entity_name, "instance", node, outStream);

	outStream << "\t// Adding children" << std::endl;
	AddChildren("instance", node, outStream);

	outStream << "}" << std::endl;
	outStream << std::endl;

}


#endif
