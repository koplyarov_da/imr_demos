#include "CodeFromXMLGeneratorBase.hpp"

#include <sc/exceptions.hpp>

#include <IMR/Beans/Types.hpp>
#include <IMR/Beans/Serialization/XMLAttributeConstructorCallDeserializer.hpp>

#include "Utils.h"


using namespace IMR::Beans;
using namespace IMR::Beans::Serialization;



std::vector<sc::shared_ptr<GeneratedCode> > CodeFromXMLGeneratorBase::Generate()
{
	struct Resetter
	{
		CodeFromXMLGeneratorBase*	_inst;
		Resetter(CodeFromXMLGeneratorBase* inst) : _inst(inst) { }
		~Resetter() { _inst->Reset(); }
	};

	Resetter resetter(this);

	const IType& root_type = Types::GetType(ToAnsiString(_rootElement->GetName()));
	const std::string root_name = ToAnsiString(_rootElement->GetAttribute(L"name"));

	SetWidgetClassName(root_name);
	AddMembers(_rootElement);
	InitMembersAndProperties(root_name, "", _rootElement);
	CallChildElementMethod("", _rootElement);

	return Flush();
}


void CodeFromXMLGeneratorBase::AddMembers(sc::shared_ptr<const sc::XML::Element> element)
{
	sc::XML::Element::ChildrenCollection::const_iterator children_it;
	for (children_it = element->GetChildren().begin(); children_it != element->GetChildren().end(); ++children_it)
	{
		const IType& child_type = Types::GetType(ToAnsiString((*children_it)->GetName()));
		AddMember(child_type, ToAnsiString((*children_it)->GetAttribute(L"name")));
		AddMembers(*children_it);
	}
}


void CodeFromXMLGeneratorBase::InitMembersAndProperties(const std::string& className, const std::string& variableName, sc::shared_ptr<const sc::XML::Element> node)
{
	const IType& type = Types::GetType(ToAnsiString(node->GetName()));
	const IClassType& class_type = dynamic_cast<const IClassType&>(type);

	const std::string entity_name = ToAnsiString(node->GetAttribute(L"name"));

	sc::XML::Element::AttributeKeysCollection::const_iterator attr_it;
	for (attr_it = node->GetAttributeNames().begin(); attr_it != node->GetAttributeNames().end(); ++attr_it)
	{
		if (*attr_it == L"name")
			continue;

		sc::shared_ptr<const IProperty> prop = class_type.GetProperty(ToAnsiString(*attr_it));
		sc::shared_ptr<const IEvent> ev = class_type.GetEvent(ToAnsiString(*attr_it));
		
		if (!prop && !ev)
			continue;
			//SC_THROW(std::runtime_error("Cannot find a property or an event '" + ToAnsiString(*attr_it) + "' in bean '" + class_type.GetName() + "'!"));
		else if (prop && ev)
			SC_THROW(std::runtime_error("Both a property and an event with a name '" + ToAnsiString(*attr_it) + "' exist in bean '" + class_type.GetName() + "'!"));

		if (prop)
		{
			const IType& property_type = prop->GetPropertyType();
			StringDataSource data_source(node->GetAttribute(*attr_it));
			sc::shared_ptr<IConstructorCall> property_value = XMLAttributeConstructorCallDeserializer(property_type).Deserialize(data_source);

			SetMemberProperty(variableName, ToAnsiString(*attr_it), property_value);
		}
		else if (ev)
		{
			AddEventHandler(variableName, ToAnsiString(*attr_it), ToAnsiString(node->GetAttribute(*attr_it)));
		}
	}

	sc::XML::Element::ChildrenCollection::const_iterator children_it;
	for (children_it = node->GetChildren().begin(); children_it != node->GetChildren().end(); ++children_it)
	{
		std::string child_name = ToAnsiString((*children_it)->GetAttribute(L"name"));
		InitMembersAndProperties(className, child_name, *children_it);
	}
}


void CodeFromXMLGeneratorBase::CallChildElementMethod(const std::string& variableName, sc::shared_ptr<const sc::XML::Element> node)
{
	const IType& type = Types::GetType(ToAnsiString(node->GetName()));
	const IClassType& class_type = dynamic_cast<const IClassType&>(type);

	sc::XML::Element::ChildrenCollection::const_iterator children_it;
	for (children_it = node->GetChildren().begin(); children_it != node->GetChildren().end(); ++children_it)
	{
		const IType& child_type = Types::GetType(ToAnsiString((*children_it)->GetName()));
		const IClassType& child_class_type = dynamic_cast<const IClassType&>(child_type);
		std::string child_name = ToAnsiString((*children_it)->GetAttribute(L"name"));

		std::set<std::wstring> unused_attributes;

		sc::XML::Element::AttributeKeysCollection::const_iterator attr_it;
		for (attr_it = (*children_it)->GetAttributeNames().begin(); attr_it != (*children_it)->GetAttributeNames().end(); ++attr_it)
		{
			if (*attr_it == L"name")
				continue;

			sc::shared_ptr<const IProperty> prop = child_class_type.GetProperty(ToAnsiString(*attr_it));
			sc::shared_ptr<const IEvent> ev = child_class_type.GetEvent(ToAnsiString(*attr_it));

			if (!prop && !ev)
				unused_attributes.insert(*attr_it);
		}

		std::vector<sc::shared_ptr<const IMethod> > marked_methods = class_type.GetMarkedMethods("childNodeMethod");
		std::vector<sc::shared_ptr<const IMethod> >::const_iterator marked_methods_it;

		int suitable_method_index = -1;

		for (size_t i = 0; i != marked_methods.size(); ++i)
		{
			std::vector<MethodParameterInfo> method_params_info = marked_methods[i]->GetParametersInfo();

			if (method_params_info.empty())
				continue;

			std::vector<MethodParameterInfo>::const_iterator params_info_it = method_params_info.begin();

			// TODO: Check first parameter type
			++params_info_it;

			bool method_is_ok = true;
			for (params_info_it; params_info_it != method_params_info.end(); ++params_info_it)
				if (unused_attributes.find(ToWideString(params_info_it->GetName())) == unused_attributes.end())
				{
					method_is_ok = false;
					break;
				}

			if (method_is_ok)
			{
				std::set<std::wstring>::const_iterator unused_attributes_it;
				for (unused_attributes_it = unused_attributes.begin(); unused_attributes_it != unused_attributes.end(); ++unused_attributes_it)
				{
					bool unused_attribute_found_in_params = false;
					for (params_info_it = method_params_info.begin(); params_info_it != method_params_info.end(); ++params_info_it)
					{
						if (ToWideString(params_info_it->GetName()) == *unused_attributes_it)
						{
							unused_attribute_found_in_params = true;
							break;
						}
					}

					if (!unused_attribute_found_in_params)
					{
						method_is_ok = false;
						break;
					}
				}
			}

			if (!method_is_ok)
				continue;

			if (suitable_method_index != -1)
				SC_THROW(std::runtime_error("Several methods are marked as 'childNodeMethod' in type '" + class_type.GetName() + "', code generator cannot choose one!"));

			suitable_method_index = (int)i;
		}

		if (suitable_method_index == -1)
			SC_THROW(std::runtime_error("Cannot find suitable method marked as 'childNodeMethod' in type '" + class_type.GetName() + "'!"));

		std::vector<sc::shared_ptr<IMethodParameter> > method_params;
		{
			std::vector<MethodParameterInfo> method_params_info = marked_methods[suitable_method_index]->GetParametersInfo();
			std::vector<MethodParameterInfo>::const_iterator params_info_it = method_params_info.begin();

			// TODO: Check first parameter type
			method_params.push_back(new MemberMethodParameter(child_name));
			++params_info_it;

			for (params_info_it; params_info_it != method_params_info.end(); ++params_info_it)
			{
				StringDataSource param_ds((*children_it)->GetAttribute(ToWideString(params_info_it->GetName())));
				sc::shared_ptr<IConstructorCall> param_val = 
					XMLAttributeConstructorCallDeserializer(params_info_it->GetType()).Deserialize(param_ds);
				method_params.push_back(new ConstructorCallMethodParameter(param_val));
			}
		}

		AddMethodCall(variableName, marked_methods[0]->GetName(), method_params);

		CallChildElementMethod(child_name, *children_it);
	}
}


