#include "CppCodeGenerator.hpp"

#include <sc/exceptions.hpp>

#include <IMR/Beans/IType.hpp>
#include <IMR/Beans/ICppTypeInfo.hpp>
#include <IMR/Beans/Serialization/CppConstructorCallSerializer.hpp>
#include <IMR/Beans/Serialization/StringDataSource.hpp>
#include <IMR/Beans/Serialization/ClassConstructorCall.hpp>
#include <IMR/Beans/Serialization/PointerTypeConstructorCall.hpp>

#include "Utils.h"


using namespace IMR::Beans;
using namespace IMR::Beans::Serialization;


namespace
{

	std::string ToCppName(const std::string& name)
	{
		std::string result = name;
		result[0] = toupper(result[0]);
		return result;
	}

	std::wstring ToCppName(const std::wstring& name)
	{
		std::wstring result = name;
		result[0] = towupper(result[0]);
		return result;
	}

}


std::vector<sc::shared_ptr<GeneratedCode> > CppCodeGenerator::Flush() const
{
	std::vector<sc::shared_ptr<GeneratedCode> > result;

	std::wstring widget_class_name = ToWideString(_widgetClassName);
	std::wstring generated_class_name = widget_class_name + L"_Designer";

	//////////////////////////////////////////////////////////////////////////

	std::wstring header;

	header += L"#ifndef __IMR_UI_CODEGEN_" + widget_class_name + L"_H__\n";
	header += L"#define __IMR_UI_CODEGEN_" + widget_class_name + L"_H__\n";
	header += L"\n\n";

	for (std::set<std::wstring>::const_iterator includes_it = _headerIncludes.begin(); includes_it != _headerIncludes.end(); ++includes_it)
		header += *includes_it + L"\n";
	if (!_headerIncludes.empty())
		header += L"\n\n";

	std::wstring indent;

	if (!_namespace.empty())
	{
		for (std::vector<std::string>::const_iterator namespace_it = _namespace.begin(); namespace_it != _namespace.end(); ++namespace_it)
		{
			header += L"namespace " + ToWideString(*namespace_it);
			if (_namespace.end() - namespace_it > 1)
				header += L" {\n";
			else
				header += L"\n{\n";
		}
		indent = L"\t";
		header += L"\n\n";
	}

	header += indent + L"class " + widget_class_name + L";\n";
	header += L"\n\n";

	header += indent + L"class " + generated_class_name + L"\n";
	header += indent + L"{\n";
	header += indent + L"protected: \n";

	for (MembersMap::const_iterator members_it = _members.begin(); members_it != _members.end(); ++members_it)
	{
		std::string namespace_str;

		const ICppTypeInfo* cpp_type_info = dynamic_cast<const ICppTypeInfo*>(&members_it->second.GetType());
		if (cpp_type_info)
			namespace_str = cpp_type_info->GetNamespace() + "::";

		std::string type_name = namespace_str + ToCppName(members_it->second.GetType().GetName());

		header += indent + L"\tsc::shared_ptr<" + ToWideString(type_name) + L"> " + ToWideString(members_it->second.GetName()) + L";\n";
	}

	header += L"\n";
	header += indent + L"protected: \n";
	header += indent + L"\tvoid InitializeWidget(" + widget_class_name + L"* instance);\n";
	header += indent + L"\tvirtual ~" + generated_class_name + L"() { }\n";
	header += indent + L"};\n";
	header += L"\n\n";

	if (!_namespace.empty())
	{
		for (std::vector<std::string>::const_iterator namespace_it = _namespace.begin(); namespace_it != _namespace.end(); ++namespace_it)
			header += L"}";

		header += L"\n\n\n";
	}

	header += L"#endif\n";

	//////////////////////////////////////////////////////////////////////////

	std::wstring source;

	source += L"#include \"" + generated_class_name + L".hpp\"\n";
	source += L"#include \"" + widget_class_name + L".hpp\"\n";
	source += L"\n";

	for (std::set<std::wstring>::const_iterator includes_it = _sourceIncludes.begin(); includes_it != _sourceIncludes.end(); ++includes_it)
		source += *includes_it + L"\n";
	if (!_sourceIncludes.empty())
		source += L"\n\n";

	source += L"\n";

	if (!_namespace.empty())
	{
		for (std::vector<std::string>::const_iterator namespace_it = _namespace.begin(); namespace_it != _namespace.end(); ++namespace_it)
		{
			source += L"namespace " + ToWideString(*namespace_it);
			if (_namespace.end() - namespace_it > 1)
				source += L" {\n";
			else
				source += L"\n{\n";
		}
		indent = L"\t";
		source += L"\n\n";
	}


	source += indent + L"void " + generated_class_name + L"::InitializeWidget(" + widget_class_name + L"* instance)\n"; 
	source += indent + L"{\n";

	if (!_widgetProperties.empty())
		source += indent + L"\t// " + widget_class_name + L" properties\n";
	for (PropertiesMap::const_iterator props_it = _widgetProperties.begin(); props_it != _widgetProperties.end(); ++props_it)
	{
		std::string property_name = ToCppName(props_it->first);

		sc::shared_ptr<const IDataSource> property_val_ds = CppConstructorCallSerializer().Serialize(*props_it->second);
		std::wstring property_val = dynamic_cast<const StringDataSource&>(*property_val_ds).GetString();

		source += indent + L"\tinstance->Set" + ToWideString(property_name) + L"(" + property_val + L");\n";
	}

	if (!_widgetEvents.empty())
		source += indent + L"\t// " + widget_class_name + L" events\n";
	for (EventsMap::const_iterator events_it = _widgetEvents.begin(); events_it != _widgetEvents.end(); ++events_it)
	{
		std::string event_name = ToCppName(events_it->first);

		source += indent + L"\tinstance->" + ToWideString(event_name) + L".connect(IMR::UI::BindThisToEventHandlerProc(instance, &" + widget_class_name + L"::" + ToWideString(events_it->second) + L"));\n";
	}

	source += L"\n";

	for (MembersMap::const_iterator members_it = _members.begin(); members_it != _members.end(); ++members_it)
	{
		std::wstring member_name = ToWideString(members_it->first);

		std::string namespace_str;

		const ICppTypeInfo* cpp_type_info = dynamic_cast<const ICppTypeInfo*>(&members_it->second.GetType());
		if (cpp_type_info)
			namespace_str = cpp_type_info->GetNamespace() + "::";

		std::string type_name = namespace_str + ToCppName(members_it->second.GetType().GetName());

		source += indent + L"\t" + member_name + L" = new " + ToWideString(type_name) + L";\n";

		const PropertiesMap& properties = members_it->second.GetPropertiesMap();
		if (!properties.empty())
			source += indent + L"\t// " + member_name + L" properties\n";
		for (PropertiesMap::const_iterator props_it = properties.begin(); props_it != properties.end(); ++props_it)
		{
			std::string property_name = ToCppName(props_it->first);

			sc::shared_ptr<const IDataSource> property_val_ds = CppConstructorCallSerializer().Serialize(*props_it->second);
			std::wstring property_val = dynamic_cast<const StringDataSource&>(*property_val_ds).GetString();

			source += indent + L"\t" + member_name + L"->Set" + ToWideString(property_name) + L"(" + property_val + L");\n";
		}

		const EventsMap& events = members_it->second.GetEvents();
		if (!events.empty())
			source += indent + L"\t// " + member_name + L" events\n";
		for (EventsMap::const_iterator events_it = events.begin(); events_it != events.end(); ++events_it)
		{
			std::string event_name = ToCppName(events_it->first);

			source += indent + L"\t" + member_name + L"->" + ToWideString(event_name) + L".connect(IMR::UI::BindThisToEventHandlerProc(instance, &" + widget_class_name + L"::" + ToWideString(events_it->second) + L"));\n";
		}

		source += L"\n";
	}

	source += indent + L"\t// Adding children\n";
	for (std::vector<MethodCall>::const_iterator methods_it = _widgetMethods.begin(); methods_it != _widgetMethods.end(); ++methods_it)
	{
		std::string method_name = ToCppName(methods_it->GetName());

		source += indent + L"\tinstance->" + ToWideString(method_name) + L"(";
		for (MethodCall::ParamsVector::const_iterator params_it = methods_it->GetParameters().begin(); params_it != methods_it->GetParameters().end(); ++params_it)
		{
			if (params_it != methods_it->GetParameters().begin())
				source += L", ";

			if (sc::InstanceOf<MemberMethodParameter>(**params_it))
			{
				const MemberMethodParameter& p = dynamic_cast<const MemberMethodParameter&>(**params_it);
				source += ToWideString(p.GetMemberName());
			}
			else if (sc::InstanceOf<ConstructorCallMethodParameter>(**params_it))
			{
				const ConstructorCallMethodParameter& p = dynamic_cast<const ConstructorCallMethodParameter&>(**params_it);
				sc::shared_ptr<IDataSource> ds = CppConstructorCallSerializer().Serialize(p.GetConstructorCall());
				source += dynamic_cast<const StringDataSource&>(*ds).GetString();
			}
		}
		source += L");\n";
	}

	for (MembersMap::const_iterator members_it = _members.begin(); members_it != _members.end(); ++members_it)
	{
		std::wstring member_name = ToWideString(members_it->first);
		for (std::vector<MethodCall>::const_iterator methods_it = members_it->second.GetMethods().begin(); methods_it != members_it->second.GetMethods().end(); ++methods_it)
		{
			std::string method_name = ToCppName(methods_it->GetName());

			source += indent + L"\t" + member_name + L"->" + ToWideString(method_name) + L"(";
			for (MethodCall::ParamsVector::const_iterator params_it = methods_it->GetParameters().begin(); params_it != methods_it->GetParameters().end(); ++params_it)
			{
				if (params_it != methods_it->GetParameters().begin())
					source += L", ";

				if (sc::InstanceOf<MemberMethodParameter>(**params_it))
				{
					const MemberMethodParameter& p = dynamic_cast<const MemberMethodParameter&>(**params_it);
					source += ToWideString(p.GetMemberName());
				}
				else if (sc::InstanceOf<ConstructorCallMethodParameter>(**params_it))
				{
					const ConstructorCallMethodParameter& p = dynamic_cast<const ConstructorCallMethodParameter&>(**params_it);
					sc::shared_ptr<IDataSource> ds = CppConstructorCallSerializer().Serialize(p.GetConstructorCall());
					source += dynamic_cast<const StringDataSource&>(*ds).GetString();
				}
			}
			source += L");\n";
		}
	}

	source += indent + L"}\n";

	if (!_namespace.empty())
	{
		source += L"\n\n";
		for (std::vector<std::string>::const_iterator namespace_it = _namespace.begin(); namespace_it != _namespace.end(); ++namespace_it)
			source += L"}";

	}

	//////////////////////////////////////////////////////////////////////////

	result.push_back(new GeneratedCode(_widgetClassName + "_Designer.hpp", header));
	result.push_back(new GeneratedCode(_widgetClassName + "_Designer.cpp", source));

	return result;
}


void CppCodeGenerator::SetWidgetClassName(const std::string& className)
{ _widgetClassName = className; }


void CppCodeGenerator::AddMember(const IMR::Beans::IType& type, const std::string& name)
{ 
	const ICppTypeInfo* cpp_type_info = dynamic_cast<const ICppTypeInfo*>(&type);
	if (cpp_type_info)
		_headerIncludes.insert(L"#include <" + ToWideString(cpp_type_info->GetIncludeFilename()) + L">");

	if (_members.find(name) != _members.end())
		SC_THROW(std::runtime_error("Member with a name '" + name + "' already exists!"));
	_members.insert(std::make_pair(name, Member(type, name))); 
}


void CppCodeGenerator::SetMemberProperty(const std::string& memberName, const std::string& propertyName, sc::shared_ptr<const IConstructorCall> propertyValue)
{
	GetIncludesFromConstructorCall(_sourceIncludes, *propertyValue);

	if (memberName.empty())
	{
		if (_widgetProperties.find(propertyName) != _widgetProperties.end())
			SC_THROW(std::runtime_error("Main widget already has a property with a name '" + propertyName + "'!"));
		_widgetProperties.insert(std::make_pair(propertyName, propertyValue));
	}
	else
	{
		MembersMap::iterator it = _members.find(memberName);
		if (it == _members.end())
			SC_THROW(std::runtime_error("Cannot find member '" + memberName + "'!"));
		it->second.AddProperty(propertyName, propertyValue);
	}
}


void CppCodeGenerator::AddMethodCall(const std::string& instanceName, const std::string& methodName, 
						   const std::vector<sc::shared_ptr<IMethodParameter> >& methodParameters)
{
	for (MethodCall::ParamsVector::const_iterator params_it = methodParameters.begin(); params_it != methodParameters.end(); ++params_it)
	{
		if (sc::InstanceOf<ConstructorCallMethodParameter>(**params_it))
			GetIncludesFromConstructorCall(_sourceIncludes, dynamic_cast<const ConstructorCallMethodParameter&>(**params_it).GetConstructorCall());
	}

	if (instanceName.empty())
		_widgetMethods.push_back(MethodCall(methodName, methodParameters));
	else
	{
		MembersMap::iterator it = _members.find(instanceName);
		if (it == _members.end())
			SC_THROW(std::runtime_error("Cannot find member '" + instanceName + "'!"));
		it->second.AddMethod(MethodCall(methodName, methodParameters));
	}
}


void CppCodeGenerator::AddEventHandler(const std::string& instanceName, const std::string& eventName, const std::string& eventHandler)
{
	if (instanceName.empty())
	{
		if (_widgetEvents.find(eventName) != _widgetEvents.end())
			SC_THROW(std::runtime_error("Main widget already has an event with a name '" + eventName + "'!"));
		_widgetEvents.insert(std::make_pair(eventName, eventHandler));
	}
	else
	{
		MembersMap::iterator it = _members.find(instanceName);
		if (it == _members.end())
			SC_THROW(std::runtime_error("Cannot find member '" + instanceName + "'!"));
		it->second.AddEvent(eventName, eventHandler);
	}
}


void CppCodeGenerator::Reset()
{
	_namespace.clear();
	_widgetClassName.clear();
	_members.clear();
	_widgetProperties.clear();
	_widgetMethods.clear();
	_widgetEvents.clear();
	_headerIncludes.clear();
	_sourceIncludes.clear();
}



void CppCodeGenerator::GetIncludesFromConstructorCall(std::set<std::wstring>& includesSet, const IConstructorCall& constructorCall)
{
	const ICppTypeInfo* cpp_type_info = dynamic_cast<const ICppTypeInfo*>(&constructorCall.GetConstructedType());
	if (cpp_type_info)
		includesSet.insert(L"#include <" + ToWideString(cpp_type_info->GetIncludeFilename()) + L">");

	const ClassConstructorCall* class_ctor_call = dynamic_cast<const ClassConstructorCall*>(&constructorCall);
	const PointerTypeConstructorCall* pointer_ctor_call = dynamic_cast<const PointerTypeConstructorCall*>(&constructorCall);

	if (class_ctor_call)
	{
		typedef sc::shared_ptr<IConstructorCall>	IConstructorCallPtr;
		typedef std::vector<IConstructorCallPtr>	IConstructorCallPtrVector;

		for (IConstructorCallPtrVector::const_iterator it = class_ctor_call->GetParameters().begin(); it != class_ctor_call->GetParameters().end(); ++it)
			GetIncludesFromConstructorCall(includesSet, **it);
	}
	if (pointer_ctor_call)
	{
		GetIncludesFromConstructorCall(includesSet, pointer_ctor_call->GetPointedTypeConstructorCall());
	}
}
