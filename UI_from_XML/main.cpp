#include <fstream>
#include <sstream>
#include <iostream>

#include <sc/XML/Reader.hpp>
#include <sc/exceptions.hpp>

#include <IMR/UI/Widgets/WidgetBeans.hpp>

#include "Utils.h"
#include "CppCodeGenerator.hpp"


void CopyStringToFile(const std::string& filename, const std::wstring& str)
{
	std::wifstream file_in(filename.c_str());
	if (file_in)
	{
		std::wstringstream file_content_stream;
		file_content_stream << file_in.rdbuf();

		if (file_content_stream.str() == str)
		{
			std::cout << "File '" << filename << "' is up-to-date." << std::endl;
			return;
		}
	}

	std::wofstream file_out(filename.c_str());
	if (!file_out)
		SC_THROW(std::runtime_error("Could not open the file'" + filename + "' for writing!"));
	file_out << str;

	std::cout << "File '" << filename << "' updated." << std::endl;
}



int main(int argc, char **argv)
{
	try
	{
		if (argc == 1)
			SC_THROW(std::runtime_error(std::string("Usage: ") + argv[0] + " <xml widget description>"));

		std::cout << "Opening input file...";
		std::wifstream inp(argv[1]);
		if (!inp)
			SC_THROW(std::runtime_error("Could not open the input file!"));
		std::cout << " Done." << std::endl;

		std::cout << "Parsing XML...";
		sc::shared_ptr<sc::XML::Element> root_node = sc::XML::Reader::Read(inp);
		std::cout << " Done." << std::endl;

		std::string header_filename = ToAnsiString(root_node->GetAttribute(L"name")) + "_Designer.hpp";
		std::string source_filename = ToAnsiString(root_node->GetAttribute(L"name")) + "_Designer.cpp";

		IMR::UI::WidgetBeans::RegisterBeans();

		std::cout << "Generating code...";
		typedef std::vector<sc::shared_ptr<GeneratedCode> >		GeneratedCodeVec;
		GeneratedCodeVec files = CppCodeGenerator(root_node).Generate();
		std::cout << " Done." << std::endl;

		std::cout << "Writing files to hard drive..." << std::endl;
		for (GeneratedCodeVec::const_iterator it = files.begin(); it != files.end(); ++it)
			CopyStringToFile((*it)->GetFilename(), (*it)->GetCode());
		std::cout << " Done." << std::endl;
	
		return 0;
	}
	catch (const std::exception& ex)
	{ std::cout << "Exception: " << ex.what() << std::endl; }
	catch (...)
	{ std::cout << "Unknown exception!" << std::endl; }

	return 1;
}
