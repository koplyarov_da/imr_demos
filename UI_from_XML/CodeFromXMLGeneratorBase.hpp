#ifndef __CODEFROMXMLGENERATORBASE_HPP__
#define __CODEFROMXMLGENERATORBASE_HPP__


#include <sc/XML/Element.hpp>
#include <IMR/Beans/Serialization/IConstructorCall.hpp>

#include "ICodeGenerator.hpp"


struct IDependency
{
	virtual ~IDependency() { }

	virtual std::wstring ToString() const = 0;
};


class CodeFromXMLGeneratorBase : public virtual ICodeGenerator
{
protected:
	struct IMethodParameter
	{ virtual ~IMethodParameter() { } };

	class MemberMethodParameter : public virtual IMethodParameter
	{ 
		std::string _memberName;

	public:
		MemberMethodParameter(const std::string& memberName) : _memberName(memberName) { }
		std::string GetMemberName() const { return _memberName; }
	};

	class ConstructorCallMethodParameter : public virtual IMethodParameter
	{ 
		typedef sc::shared_ptr<IMR::Beans::Serialization::IConstructorCall>		IConstructorCallPtr;
		IConstructorCallPtr _constructorCall;

	public:
		ConstructorCallMethodParameter(IConstructorCallPtr constructorCall) : _constructorCall(constructorCall) { }
		const IMR::Beans::Serialization::IConstructorCall& GetConstructorCall() const { return *_constructorCall; } 
	};

private:
	sc::shared_ptr<const sc::XML::Element>	_rootElement;

protected:
	CodeFromXMLGeneratorBase(sc::shared_ptr<const sc::XML::Element> rootElement)
		: _rootElement(rootElement)
	{ }

public:
	virtual std::vector<sc::shared_ptr<GeneratedCode> > Generate();

	virtual std::vector<sc::shared_ptr<GeneratedCode> > Flush() const = 0;

	virtual void SetWidgetClassName(const std::string& className) = 0;
	virtual void AddMember(const IMR::Beans::IType& type, const std::string& name) = 0;

	virtual void SetMemberProperty(const std::string& memberName, const std::string& propertyName, 
		sc::shared_ptr<const IMR::Beans::Serialization::IConstructorCall> propertyValue) = 0;

	// events etc.

	virtual void AddMethodCall(const std::string& instanceName, const std::string& methodName, 
		const std::vector<sc::shared_ptr<IMethodParameter> >& methodParameters) = 0;

	virtual void AddEventHandler(const std::string& instanceName, const std::string& eventName, const std::string& eventHandler) = 0;

	virtual void Reset() = 0;

private:
	void AddMembers(sc::shared_ptr<const sc::XML::Element> element);
	void InitMembersAndProperties(const std::string& className, const std::string& variableName, sc::shared_ptr<const sc::XML::Element> node);
	void CallChildElementMethod(const std::string& variableName, sc::shared_ptr<const sc::XML::Element> node);
};


#endif
