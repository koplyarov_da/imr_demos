#ifndef __IGENERATEDCODE_HPP__
#define __IGENERATEDCODE_HPP__


#include <string>


class GeneratedCode
{
private:
	std::string		_filename;
	std::wstring	_code;

public:
	GeneratedCode(const std::string& filename, const std::wstring& code)
		: _filename(filename), _code(code)
	{ }

	const std::string& GetFilename() const	{ return _filename; }
	const std::wstring& GetCode() const		{ return _code; }
};


#endif