#ifndef __CPPCODEGENERATOR_HPP__
#define __CPPCODEGENERATOR_HPP__


#include <set>

#include <sc/exceptions.hpp>
#include <sc/shared_ptr.hpp>

#include <IMR/Beans/Serialization/IConstructorCall.hpp>

#include "CodeFromXMLGeneratorBase.hpp"


class CppCodeGenerator : public CodeFromXMLGeneratorBase
{
private:
	typedef sc::shared_ptr<const IMR::Beans::Serialization::IConstructorCall>	IConstructorCallPtr;
	typedef std::map<std::string, IConstructorCallPtr>								PropertiesMap;
	typedef std::map<std::string, std::string>										EventsMap;

	class MethodCall
	{
	public:
		typedef std::vector<sc::shared_ptr<IMethodParameter> >	ParamsVector;

	private:
		std::string		_name;
		ParamsVector	_parameters;

	public:
		MethodCall(const std::string& name, const ParamsVector& parameters)
			: _name(name), _parameters(parameters)
		{ }

		const std::string& GetName() const			{ return _name; }
		const ParamsVector& GetParameters() const	{ return _parameters; }
	};

	class Member
	{
	private:
		const IMR::Beans::IType*		_type;
		const std::string				_name;
		PropertiesMap					_properties;
		std::vector<MethodCall>			_methods;
		EventsMap						_events;

	public:
		Member(const IMR::Beans::IType& type, const std::string& name)
			: _type(&type), _name(name)
		{ }

		const IMR::Beans::IType& GetType() const	{ return *_type;}
		std::string GetName() const						{ return _name; }

		void AddProperty(const std::string& propertyName, IConstructorCallPtr propertyValue)
		{ 
			if (_properties.find(propertyName) != _properties.end())
				SC_THROW(std::runtime_error("Widget '" + _name + "' already has a property with a name '" + propertyName + "'!"));
			_properties.insert(std::make_pair(propertyName, propertyValue)); 
		}

		void AddMethod(const MethodCall& method)
		{ _methods.push_back(method); }

		void AddEvent(const std::string& eventName, const std::string& eventHandler)
		{ 
			if (_events.find(eventName) != _events.end())
				SC_THROW(std::runtime_error("Widget '" + _name + "' already has an event with a name '" + eventName + "'!"));
			_events.insert(std::make_pair(eventName, eventHandler)); 
		}

		const PropertiesMap& GetPropertiesMap() const		{ return _properties; }
		const std::vector<MethodCall>& GetMethods() const	{ return _methods; }
		const EventsMap& GetEvents() const					{ return _events; }
	};

	typedef std::map<std::string, Member>	MembersMap;

private:
	std::vector<std::string>	_namespace;
	std::string					_widgetClassName;
	MembersMap					_members;
	std::vector<MethodCall>		_widgetMethods;
	PropertiesMap				_widgetProperties;
	EventsMap					_widgetEvents;
	std::set<std::wstring>		_headerIncludes;
	std::set<std::wstring>		_sourceIncludes;

public:
	CppCodeGenerator(sc::shared_ptr<const sc::XML::Element> rootElement)
		: CodeFromXMLGeneratorBase(rootElement)
	{ }

	virtual std::vector<sc::shared_ptr<GeneratedCode> > Flush() const;

	virtual void SetWidgetClassName(const std::string& className);
	virtual void AddMember(const IMR::Beans::IType& type, const std::string& name);

	virtual void SetMemberProperty(const std::string& memberName, const std::string& propertyName, 
		sc::shared_ptr<const IMR::Beans::Serialization::IConstructorCall> propertyValue);

	virtual void AddMethodCall(const std::string& instanceName, const std::string& methodName, 
		const std::vector<sc::shared_ptr<IMethodParameter> >& methodParameters);

	virtual void AddEventHandler(const std::string& instanceName, const std::string& eventName, const std::string& eventHandler);

	virtual void Reset();

private:
	static void GetIncludesFromConstructorCall(std::set<std::wstring>& includesSet, const IMR::Beans::Serialization::IConstructorCall& constructorCall);
};


#endif
