#ifndef __GENERATEHEADERFILE_H__
#define __GENERATEHEADERFILE_H__

#include <iostream>
#include <set>
#include <string>

#include <sc/XML/Reader.hpp>

#include <IMR/UI/Beans/BeansManager.hpp>

#include "Utils.h"


void UpdateIncludes(std::set<std::string>& includeFiles, sc::shared_ptr<sc::XML::Element> node)
{
	const IMR::UI::Bean& bean = IMR::UI::BeansManager::GetBean(ToAnsiString(node->GetName()));
	if (includeFiles.find(bean.GetCppHeader()) == includeFiles.end())
		includeFiles.insert(bean.GetCppHeader());

	sc::XML::Element::ChildrenCollection::const_iterator children_it;
	for (children_it = node->GetChildren().begin(); children_it != node->GetChildren().end(); ++children_it)
		UpdateIncludes(includeFiles, *children_it);
}


void AddMembers(sc::shared_ptr<sc::XML::Element> node, std::ostream& outStream)
{
	sc::XML::Element::ChildrenCollection::const_iterator children_it;
	for (children_it = node->GetChildren().begin(); children_it != node->GetChildren().end(); ++children_it)
	{
		const IMR::UI::Bean& child_bean = IMR::UI::BeansManager::GetBean(ToAnsiString((*children_it)->GetName()));
		outStream << "\t" << "sc::shared_ptr<" << child_bean.GetCppTypeName() << ">\t\t" << ToAnsiString((*children_it)->GetAttribute(L"name")) << ";" << std::endl;
		AddMembers(*children_it, outStream);
	}
}


void GenerateHeaderFile(sc::shared_ptr<sc::XML::Element> node, std::ostream& outStream)
{
	const IMR::UI::Bean& bean = IMR::UI::BeansManager::GetBean(ToAnsiString(node->GetName()));

	std::set<std::string> include_files;

	UpdateIncludes(include_files, node);

	std::string entity_name = ToAnsiString(node->GetAttribute(L"name"));

	outStream << "#ifndef __IMR_UI_CODEGEN_" << entity_name << "_H__" << std::endl;
	outStream << "#define __IMR_UI_CODEGEN_" << entity_name << "_H__" << std::endl;
	outStream << std::endl << std::endl;

	std::set<std::string>::const_iterator includes_it;
	for (includes_it = include_files.begin(); includes_it != include_files.end(); ++includes_it)
		outStream << "#include <" << *includes_it << ">" << std::endl;
	outStream << std::endl << std::endl;

	outStream << "class " << entity_name << ";" << std::endl;
	outStream << std::endl << std::endl;

	outStream << "class " << entity_name << "_Designer" << std::endl;
	outStream << "{" << std::endl;
	outStream << "protected:" << std::endl;

	AddMembers(node, outStream);

	outStream << std::endl;
	outStream << "protected:" << std::endl;
	outStream << "\tvoid InitializeWidget(" << /*bean.GetCppTypeName()*/entity_name << "* instance);" << std::endl;
	outStream << "\tvirtual ~" << entity_name << "_Designer() { }" << std::endl;
	outStream << "};" << std::endl;
	outStream << std::endl << std::endl;

	/*
	outStream << "template < typename BaseClass = " << bean.GetCppTypeName() << " >" << std::endl;
	outStream << "class " << entity_name << "_Designer : public BaseClass, protected " << entity_name << "_DesignerBase" << std::endl;
	outStream << "{" << std::endl;
	outStream << "protected:" << std::endl;
	outStream << "\t" << entity_name << "_Designer()" << std::endl;
	outStream << "\t{ " << entity_name << "_DesignerBase::InitializeWidget(this); }" << std::endl;
	outStream << "};" << std::endl;
	outStream << std::endl << std::endl;
	*/
	outStream << "#endif" << std::endl;
}


#endif
