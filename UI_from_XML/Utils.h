#ifndef __UTILS_H__
#define __UTILS_H__


#include <algorithm>
#include <string>

#include <sc/exceptions.hpp>


inline char ToAnsiChar(wchar_t c)
{
	if (c > 127)
		SC_THROW(std::runtime_error("Invalid character!"));
	return (char)c;
}

inline std::string ToAnsiString(const std::wstring& wstr)
{
	if (wstr.empty())
		SC_THROW(std::runtime_error("Empty string!")); // wtf?
	std::string result;
	result.reserve(wstr.size());
	std::transform(wstr.begin(), wstr.end(), std::back_inserter(result), std::ptr_fun(&ToAnsiChar));
	return result;
}


inline wchar_t ToWideChar(char c)
{
	if (c > 127)
		SC_THROW(std::runtime_error("Invalid character!"));
	return (wchar_t)c;
}

inline std::wstring ToWideString(const std::string& str)
{
	if (str.empty())
		SC_THROW(std::runtime_error("Empty string!")); // wtf?
	std::wstring result;
	result.reserve(str.size());
	std::transform(str.begin(), str.end(), std::back_inserter(result), std::ptr_fun(&ToWideChar));
	return result;
}

#endif
