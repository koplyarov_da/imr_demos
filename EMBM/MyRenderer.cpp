#include <iostream>

#include <IMR/GFX/Auxiliary/Textures.hpp>
#include <IMR/GFX/Auxiliary/Shaders.hpp>
#include <IMR/GFX/Auxiliary/Skybox.hpp>

#include "helper/figures.hpp"

#include "MyRenderer.hpp"


using namespace sc::Math;
using namespace IMR::GFX;
using namespace IMR::GFX::API;


const RendererType g_rendererType = 
#if defined(_WIN32)
	IG::RendererType::Direct3D9;
#else
	IG::RendererType::OpenGL;
#endif


namespace Detail
{

	template < typename PixelType >
	class NormalMapBuilderHelper
	{
	private:
		const Auxiliary::Image<PixelType>&	_heightMap;
		float								_k;

	public:
		NormalMapBuilderHelper(const Auxiliary::Image<PixelType>& heightMap, float k)
			: _heightMap(heightMap), _k(k)
		{ }

		Vector3D_f ToHeightVec(int i, int j)
		{
			if (i < 0) i = 0;
			if (j < 0) j = 0;
			if (i >= _heightMap.GetWidth() - 1)  i = _heightMap.GetWidth() - 1;
			if (j >= _heightMap.GetHeight() - 1) j = _heightMap.GetHeight() - 1;
			RGBAPixelData height_texel = _heightMap(i, j).GetRGBAPixelData();
			return Vector3D_f((float)i, (float)j, height_texel.GetFloat32Luminance() * _k);
		}
	};

}

template < typename DestPixelType, typename SrcPixelType >
Auxiliary::Image<DestPixelType> HeightMapToNormalMap(const Auxiliary::Image<SrcPixelType>& heightMap)
{
	using namespace IMR::GFX::Auxiliary;
	Image<DestPixelType> result(heightMap.GetWidth(), heightMap.GetHeight());
	::Detail::NormalMapBuilderHelper<SrcPixelType> hlp(heightMap, 1);

	for (int j = 0; j < (int)heightMap.GetHeight(); ++j)
		for (int i = 0; i < (int)heightMap.GetWidth(); ++i)
		{
			Vector3D_f n1 = -Vector3D_f::CrossProduct(hlp.ToHeightVec(i-1, j) - hlp.ToHeightVec(i+1, j), 
													  hlp.ToHeightVec(i, j-1) - hlp.ToHeightVec(i, j+1)).Normalize();
			Vector3D_f n2 = -Vector3D_f::CrossProduct(hlp.ToHeightVec(i-1, j-1) - hlp.ToHeightVec(i+1, j+1), 
													  hlp.ToHeightVec(i+1, j-1) - hlp.ToHeightVec(i-1, j+1)).Normalize();
			Vector3D_f n = (n1 + n2).Normalize();
			n = n * 0.5f + Vector3D_f(0.5f, 0.5f, 0.5f);

			result(i, j) = RGBAPixelData(n.X, n.Y, n.Z, 0);
		}

	return result;
}


Matrix_f GetNormalMatrix(const Matrix_f& worldMatrix)
{
	Vector3D_f v0(worldMatrix(0, 0), worldMatrix(1, 0), worldMatrix(2, 0));
	Vector3D_f v1(worldMatrix(0, 1), worldMatrix(1, 1), worldMatrix(2, 1));
	Vector3D_f v2(worldMatrix(0, 2), worldMatrix(1, 2), worldMatrix(2, 2));

	Vector3D_f new_v0 = v0 * Sqr(v1.GetLength()) * Sqr(v2.GetLength());
	Vector3D_f new_v1 = v1 * Sqr(v2.GetLength()) * Sqr(v0.GetLength());
	Vector3D_f new_v2 = v2 * Sqr(v0.GetLength()) * Sqr(v1.GetLength());

	Matrix_f result = Matrix_f::Identity();
	result(0, 0) = new_v0.X;
	result(1, 0) = new_v0.Y;
	result(2, 0) = new_v0.Z;
	result(0, 1) = new_v1.X;
	result(1, 1) = new_v1.Y;
	result(2, 1) = new_v1.Z;
	result(0, 2) = new_v2.X;
	result(1, 2) = new_v2.Y;
	result(2, 2) = new_v2.Z;
	return result;
}


MyRenderer::MyRenderer(const helper::Window& wnd)
	:	base(g_rendererType, wnd.GetRendererPlaceholder()), 
		_animationSpeed(1 / 10.0f), 
		_camRadius(10)
{ 
	SetRenderState<RenderState::ZEnable>(true);
	SetRenderState<RenderState::ZWriteEnable>(true);
	SetRenderState<RenderState::CullMode>(CullMode::Clockwise);
	SetRenderState<RenderState::NormalizeNormals>(true);

	_shader = Auxiliary::LoadHLShaderFromFiles(*this, "./Shaders/" + g_rendererType.ToString() + "/embm.vsh", "./Shaders/" + g_rendererType.ToString() + "/embm.psh");

	//_diffTexSampler		= _shader->GetConstantValue<TextureSampler>("diffuseMap");
	_specTexSampler			= _shader->GetConstantValue<TextureSampler>("specularMap");
	_bumpTexSampler			= _shader->GetConstantValue<TextureSampler>("bumpMap");
	_envTexSampler			= _shader->GetConstantValue<TextureSampler>("envMap");

	_eyePos					= _shader->GetConstant<Vector3D_f>("eyePos"); 
	_normalMatrix			= _shader->GetConstant<Matrix_f>("normalMatrix");
	_worldMatrix			= _shader->GetConstant<Matrix_f>("world");
	_worldViewProjMatrix	= _shader->GetConstant<Matrix_f>("worldViewProj");

	std::string cubemaps = "./Cubemaps/lnd_";
	std::string cubemaps_ext = ".bmp";
	Auxiliary::Image<B8G8R8A8> xpos(cubemaps + "xpos" + cubemaps_ext);
	Auxiliary::Image<B8G8R8A8> xneg(cubemaps + "xneg" + cubemaps_ext);
	Auxiliary::Image<B8G8R8A8> ypos(cubemaps + "ypos" + cubemaps_ext);
	Auxiliary::Image<B8G8R8A8> yneg(cubemaps + "yneg" + cubemaps_ext);
	Auxiliary::Image<B8G8R8A8> zpos(cubemaps + "zpos" + cubemaps_ext);
	Auxiliary::Image<B8G8R8A8> zneg(cubemaps + "zneg" + cubemaps_ext);
	_skyboxTex = CreateCubeTexture<B8G8R8A8>(xpos.GetWidth(), TextureSettings(TextureFilter::Linear, TextureFilter::Linear, MipmapFilter::None));
	_skyboxTex->SetData(CubemapFace::PositiveX, 0, xpos.GetData());
	_skyboxTex->SetData(CubemapFace::NegativeX, 0, xneg.GetData());
	_skyboxTex->SetData(CubemapFace::PositiveY, 0, ypos.GetData());
	_skyboxTex->SetData(CubemapFace::NegativeY, 0, yneg.GetData());
	_skyboxTex->SetData(CubemapFace::PositiveZ, 0, zpos.GetData());
	_skyboxTex->SetData(CubemapFace::NegativeZ, 0, zneg.GetData());
	_envTex = _skyboxTex;

	Auxiliary::Image<B8G8R8A8> height_map("./Textures/_bump_map.bmp");
	_bumpTex = Auxiliary::LoadTextureFromImage<B8G8R8A8>(*this, HeightMapToNormalMap<B8G8R8A8>(height_map));
	_diffTex = Auxiliary::LoadTextureFromFile<B8G8R8A8>(*this, "./Textures/empty.bmp");
	_specTex = Auxiliary::LoadTextureFromFile<B8G8R8A8>(*this, "./Textures/glass.bmp");

	_skybox = Auxiliary::CreateSkybox(*this);
	_mesh = helper::CreateSphere(*this, 5);
}


void MyRenderer::OnRender()
{
	float t = _timer.Get() * _animationSpeed;	

	_eye.SetFOV(90);
	_eye.SetPos(Vector3D_f(Sin(t), 0.3f * Sin(4 * t), -Cos(t)) * _camRadius);
	_eye.SetDir(-_eye.GetPos());
	LookAt(_eye);

	ClearBuffers(BufferType::ColorBuffer | BufferType::ZBuffer);
	
	
	DeactivateTexture(_specTexSampler);
	DeactivateTexture(_bumpTexSampler);
	DeactivateTexture(_envTexSampler);
	DeactivateShader();

	SetRenderState<RenderState::ZEnable>(false);
	SetRenderState<RenderState::ZWriteEnable>(false);
	SetTransform(TransformMode::World, Matrix_f::Scale(10) * Matrix_f::Translate(_eye.GetPos()));
	_skyboxTex->MakeActive(0);
	_skybox->Render();

	
	SetRenderState<RenderState::ZEnable>(true);
	SetRenderState<RenderState::ZWriteEnable>(true);
	SetTransform(TransformMode::World, Matrix_f::Scale(1, 0.8f, 0.2f));
	//_diffTex->MakeActive(_diffTexSampler);
	_specTex->MakeActive(_specTexSampler);
	_bumpTex->MakeActive(_bumpTexSampler);
	_envTex->MakeActive(_envTexSampler);
	_shader->MakeActive();

	_eyePos->Value = _eye.GetPos();
	_normalMatrix->Value = GetNormalMatrix(GetTransform(TransformMode::World));
	_worldMatrix->Value = GetTransform(TransformMode::World);
	_worldViewProjMatrix->Value = GetTransform(TransformMode::World) * 
								  GetTransform(TransformMode::View) * 
								  GetTransform(TransformMode::Projection);
								  

	_mesh->Render();
}
