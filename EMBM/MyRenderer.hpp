#pragma once

#include <sc/shared_ptr.hpp>
#include <sc/Timer.hpp>

#include <IMR/GFX/Renderer.hpp>

#include "helper/Window.hpp"


namespace IG = IMR::GFX;


class MyRenderer : public IMR::GFX::Renderer
{
private:
	typedef IG::Renderer									base;
	typedef sc::shared_ptr<IG::IRenderable>					IRenderablePtr;
	typedef sc::shared_ptr<IG::Texture2D<IG::B8G8R8A8> >	Texture2DPtr;
	typedef sc::shared_ptr<IG::CubeTexture<IG::B8G8R8A8> >	CubeTexturePtr;
	typedef sc::shared_ptr<IG::HLShader>					HLShaderPtr;
	typedef IG::HLShaderConstant<IG::Matrix_f>				MatrixHLShaderConst;
	typedef IG::HLShaderConstant<IG::Vector3D>				VectorHLShaderConst;
	typedef sc::shared_ptr<MatrixHLShaderConst>				MatrixHLShaderConstPtr;
	typedef sc::shared_ptr<VectorHLShaderConst>				VectorHLShaderConstPtr;


	const float				_animationSpeed;
	const float				_camRadius;

	IG::Eye					_eye;
	
	IRenderablePtr			_skybox;
	IRenderablePtr			_mesh;
	
	CubeTexturePtr			_skyboxTex;

	CubeTexturePtr			_envTex;
	Texture2DPtr			_diffTex;
	Texture2DPtr			_specTex;
	Texture2DPtr			_bumpTex;

	int						_envTexSampler;
	int						_diffTexSampler;
	int						_specTexSampler;
	int						_bumpTexSampler;
	
	HLShaderPtr				_shader;
	MatrixHLShaderConstPtr	_worldViewProjMatrix;
	MatrixHLShaderConstPtr	_worldMatrix;
	MatrixHLShaderConstPtr	_normalMatrix;
	VectorHLShaderConstPtr	_eyePos;

	sc::Timer				_timer;

public:
	MyRenderer(const helper::Window& wnd);

	virtual void OnRender();
};
