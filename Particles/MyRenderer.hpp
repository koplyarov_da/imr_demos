#pragma once

#include <sc/shared_ptr.hpp>
#include <sc/Timer.hpp>

#include <IMR/GFX/Renderer.hpp>

#include "helper/Window.hpp"
#include "ParticleSystem.hpp"


namespace IG = IMR::GFX;


struct Particle
{
	IG::Vector3D	Position;
	IG::Vector3D	Velocity;
	IG::B8G8R8A8	Color;

	Particle(const IG::Vector3D& position, const IG::Vector3D& velocity, IG::B8G8R8A8 color)
		: Position(position), Velocity(velocity), Color(color)
	{ }
};


class MyRenderer : public IMR::GFX::Renderer
{
	friend class ParticleRenderer;

public:
	typedef IG::VertexDiffuse						ParticleVertex;
	typedef IG::VertexArray<ParticleVertex>			ParticlesVertexArray;

private:
	typedef IG::Renderer							base;
	typedef sc::shared_ptr<ParticlesVertexArray>	ParticlesVertexArrayPtr;
	typedef sc::shared_ptr<IG::ITexture>			ITexturePtr;

	ITexturePtr					_particleTex;
	ParticlesVertexArrayPtr		_particles;
	ParticleSystem<Particle>	_particleSystem;
	float						_prevTime;
	sc::Timer					_timer;
	int							_colorIndex;
	IG::Vector3D				_position;
	float						_direction;

public:
	MyRenderer(const helper::Window& wnd);

	virtual void OnRender();
};
