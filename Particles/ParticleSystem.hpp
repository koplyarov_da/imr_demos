#ifndef __PARTICLESYSTEM_HPP__
#define __PARTICLESYSTEM_HPP__


#include <algorithm>
#include <list>

#include <IMR/GFX/Types.hpp>


template < typename ParticleType >
class ParticleSystem
{
	typedef std::vector<ParticleType>		ParticlesList;

	template < typename ParticleProcessFunc >
	class NegateParticleProcessFunc
	{
	private:
		ParticleProcessFunc		_f;

	public:
		NegateParticleProcessFunc(const ParticleProcessFunc& f) : _f(f) { }
		bool operator () (ParticleType& p) const { return !_f(p); }
	};

private:
	ParticlesList			_particles;

public:
	size_t GetCount() const { return _particles.size(); }

	void AddParticle(const ParticleType& particle)
	{ _particles.push_back(particle); }

	template < typename ParticleProcessFunc >
	void Process(const ParticleProcessFunc& processFunc)		
	{ _particles.erase(std::remove_if(_particles.begin(), _particles.end(), NegateParticleProcessFunc<ParticleProcessFunc>(processFunc)), _particles.end()); }

	template < typename ParticleRenderFunc >
	void Render(const ParticleRenderFunc& renderFunc) const	
	{ std::for_each(_particles.begin(), _particles.end(), renderFunc); }
};


#endif
