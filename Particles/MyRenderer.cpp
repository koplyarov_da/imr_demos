#include <stdlib.h>

#include <IMR/GFX/Auxiliary/Textures.hpp>
#include <IMR/GFX/Auxiliary/Shaders.hpp>

#include <iostream>

#include "helper/figures.hpp"

#include "MyRenderer.hpp"

#undef min
#undef max


using namespace IMR::GFX;


const RendererType g_rendererType = 
#ifdef _WIN32
	IG::RendererType::Direct3D9;
#else
	IG::RendererType::OpenGL;
#endif


MyRenderer::MyRenderer(const helper::Window& wnd)
	:	base(g_rendererType, wnd.GetRendererPlaceholder()),
		_prevTime(0), _colorIndex(0), _direction(sc::Math::PI_f / 4)
{ 
	SetRenderState<RenderState::Blend>(true);
	SetRenderState<RenderState::SrcBlend>(BlendFactor::One);
	SetRenderState<RenderState::DestBlend>(BlendFactor::One);
	SetRenderState<RenderState::PointSpriteEnable>(true);
	SetRenderState<RenderState::PointSizeMin>(1);
	SetRenderState<RenderState::PointSizeMax>(50);
	SetRenderState<RenderState::PointSize>(30);

	_particles = CreateVertexArray<ParticleVertex>(DrawMode::Points, ArrayUsage::DynamicDraw, ArrayAccess::WriteOnly);
	_particles->Reserve(10000);

	_particleTex = Auxiliary::LoadTextureFromFile<B8G8R8A8>(*this, "./Textures/particle.bmp");
	_particleTex->MakeActive(0);
}


class ParticleRenderer
{
private:
	MyRenderer::ParticlesVertexArray::LockedData&	_vaData;
	mutable size_t									_ofs;

public:
	ParticleRenderer(MyRenderer::ParticlesVertexArray::LockedData& vaData)
		: _vaData(vaData), _ofs(0)
	{ }

	void operator () (const Particle& particle) const 
	{ _vaData[_ofs++] = MyRenderer::ParticleVertex(particle.Position, particle.Color); }
};

class ParticleProcessor
{
private:
	const Renderer&		_renderer;
	const float			_dt;

public:
	ParticleProcessor(const Renderer& renderer, float dt)
		: _renderer(renderer), _dt(dt)
	{ }

	bool operator () (Particle& particle) const 
	{
		Vector3D a(0, -50, 0);
		particle.Velocity += a * _dt;
		particle.Position += particle.Velocity * _dt + a * _dt * _dt / 2;
		return particle.Position.Y >= -(int)_renderer.GetHeight() / 2;
	}
};


void MyRenderer::OnRender()
{
	float time = _timer.Get();
	float dt = time - _prevTime;
	const float velocity_limit = 50.f;

	_prevTime = time;
	SetTransform(TransformMode::Projection, Matrix_f::Scale(2.f / GetWidth(), 2.f / GetHeight(), 1));

	ClearBuffers(BufferType::ColorBuffer | BufferType::ZBuffer);

	_position.X = sc::Math::Max(_position.X, -(int)GetWidth() / 2.f);
	_position.X = sc::Math::Min(_position.X, GetWidth() / 2.f);
	_position.Y = sc::Math::Max(_position.Y, -(int)GetHeight() / 2.f);
	_position.Y = sc::Math::Min(_position.Y, GetHeight() / 2.f);

	_direction += (rand() % 512 - 255) / 128.f * dt;
	Vector3D velocity = Vector3D(sc::Math::Cos(_direction), sc::Math::Sin(_direction), 0) * 100;
	Vector3D next_pos = _position + velocity * dt;
	if (next_pos.X < -(int)GetWidth() / 2 || next_pos.X > GetWidth() / 2)
		_direction = sc::Math::PI_f - _direction;
	if (next_pos.Y < -(int)GetHeight() / 2 || next_pos.Y > GetHeight() / 2)
		_direction = -_direction;
	velocity = Vector3D(sc::Math::Cos(_direction), sc::Math::Sin(_direction), 0) * 100;
	_position += velocity * dt;

	for (int q = 0; q < 30; ++q)
	{
		int i = (++_colorIndex) % 6;
		float angle = (rand() % 512) / 512.f * (2 * sc::Math::PI_f);
		float velocity = (sc::Math::Sqrt(float(rand() % 65536)) + 0.5f) / 3.f; // linear distribution [0..128]
		_particleSystem.AddParticle(Particle(_position, 
			Vector3D(sc::Math::Cos(angle), sc::Math::Sin(angle), 0) * velocity, 
			B8G8R8A8((i == 0 || i == 3 || i == 4) ? 255 : 0, (i == 1 || i == 3 || i == 5) ? 255 : 0, (i == 2 || i == 4 || i == 5) ? 255 : 0)));
	}

	_particleSystem.Process(ParticleProcessor(*this, dt));

	size_t size = _particleSystem.GetCount();
	if (size > 0)
	{
		_particles->Reserve(size);
		{
			ParticlesVertexArray::LockedData l = _particles->Lock(0, size, ArrayAccess::DiscardData);
			_particleSystem.Render(ParticleRenderer(l));
		}
		_particles->Render(size);
	}
}
