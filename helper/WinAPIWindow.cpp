#ifdef _WIN32


#include <sc/CT/ValToValMap.hpp>
#include <sc/exceptions.hpp>

#include "helper/WinAPIWindow.hpp"


#ifndef WS_EX_LAYERED
#define WS_EX_LAYERED	0x80000 
#endif

#ifndef LWA_ALPHA
#define LWA_ALPHA		0x00000002
#endif

namespace helper
{

	typedef sc::CT::ValToValMap_1
		<
			WPARAM, Key::Values, 
			SC_CT_PAIR( VK_ESCAPE, Key::Escape )
		> KeyMappings;


	std::map<HWND, Window*> Window::handleToWindowMap_;


	Window::Window(int width, int height)
	{
		HINSTANCE instance = NULL;
		int cmdShow = SW_MAXIMIZE;
		LPCWSTR wndClassName = L"WindowClass";
		LPCWSTR wndName = L"���� � ������";

		windowClass_.hInstance = instance;
		windowClass_.lpszClassName = wndClassName;
		windowClass_.lpfnWndProc = WindowFunc;
		windowClass_.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;

		windowClass_.hIcon = NULL;
		windowClass_.hCursor = LoadCursor(NULL,IDC_ARROW);
		windowClass_.lpszMenuName = NULL;

		windowClass_.cbClsExtra = 0;
		windowClass_.cbWndExtra = 0;

		windowClass_.hbrBackground = 0; //(HBRUSH)GetStockObject(BLACK_BRUSH);
		RegisterClassW(&windowClass_);

		wndHandle_ = CreateWindowW(
			wndClassName, 
			wndName, 
			WS_CAPTION | WS_SYSMENU | WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			width,
			height,
			HWND_DESKTOP, NULL,
			instance, (LPVOID)this);

		/*
		// Transparency
		SetWindowLong(wndHandle_, GWL_EXSTYLE, GetWindowLong(wndHandle_, GWL_EXSTYLE) | WS_EX_LAYERED);

		typedef DWORD (WINAPI *PSLWA)(HWND, DWORD, BYTE, DWORD);
		
		HMODULE hDLL = LoadLibraryA("user32");
		PSLWA pSetLayeredWindowAttributes = (PSLWA) GetProcAddress(hDLL,"SetLayeredWindowAttributes");
		
		if (pSetLayeredWindowAttributes != NULL) 
			pSetLayeredWindowAttributes(wndHandle_, RGB(255, 255, 255), 155, LWA_ALPHA);
		*/

		cmdShow_ = cmdShow;
	}


	Window::~Window()
	{
		KillWindow();
	}


	void Window::KillWindow()
	{
		HandleToWindowMap::iterator iter = handleToWindowMap_.find(wndHandle_);
		handleToWindowMap_.erase(iter);
	}


	void Window::SetText(const std::string& text)
	{ ::SetWindowTextA(wndHandle_, text.c_str()); }

	void Window::Show()
	{
		::ShowWindow(GetHandle(), cmdShow_);
		if (::UpdateWindow(GetHandle()) == FALSE) SC_THROW(std::runtime_error("UpdateWindow failed."));
	}

	void Window::Quit()
	{
		PostQuitMessage(0); 
	}

	HFONT Window::GetFont() const
	{
		HFONT result = 
			CreateFontW(16,								// Height
						0,								// Width
						0,								// Escapement
						0,								// Orientation
						FW_BOLD,						// Weight
						FALSE,							// Italic
						FALSE,							// Underline
						0,								// StrikeOut
						ANSI_CHARSET,					// CharSet
						OUT_DEFAULT_PRECIS,				// OutPrecision
						CLIP_DEFAULT_PRECIS,			// ClipPrecision
						DEFAULT_QUALITY,				// Quality
						DEFAULT_PITCH | FF_SWISS,		// PitchAndFamily
						L"Arial");						// Facename

		return result;
	}


	LRESULT CALLBACK Window::WindowFunc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) 
	{
		HandleToWindowMap::iterator iter = handleToWindowMap_.find(hWnd);
		
		Window *window;
		if (msg == WM_CREATE)
		{
			LPCREATESTRUCT cs = (LPCREATESTRUCT)lParam;
			window = (Window*)cs->lpCreateParams;
			handleToWindowMap_.insert(HandleToWindowPair(hWnd, window));
			return 0;
		}
		
		if (iter == handleToWindowMap_.end()) return DefWindowProc(hWnd, msg, wParam, lParam);
		window = (*iter).second;

		switch (msg)
		{
			case WM_ACTIVATE:	window->OnActivate();		break;		
			case WM_DESTROY:	
				window->OnDestroy();		
				break;		
			case WM_PAINT:		window->OnPaint();			break;		
			case /*WM_MOUSEWHEEL*/0x020A: 
				{
					POINT p = { LOWORD(lParam), HIWORD(lParam) };
					ScreenToClient(hWnd, &p);
					window->OnMouseWheel(p.x, p.y, short(HIWORD(wParam))); 
				}
				break;
			case WM_LBUTTONUP:	
				window->OnMouseUp(LOWORD(lParam), HIWORD(lParam), IMR::UI::MouseButton::Left);		
				break;
			case WM_LBUTTONDOWN:	
				window->OnMouseDown(LOWORD(lParam), HIWORD(lParam), IMR::UI::MouseButton::Left);		
				break;
			case WM_MBUTTONUP:	
				window->OnMouseUp(LOWORD(lParam), HIWORD(lParam), IMR::UI::MouseButton::Middle);		
				break;
			case WM_MBUTTONDOWN:	
				window->OnMouseDown(LOWORD(lParam), HIWORD(lParam), IMR::UI::MouseButton::Middle);		
				break;
			case WM_RBUTTONUP:	
				window->OnMouseUp(LOWORD(lParam), HIWORD(lParam), IMR::UI::MouseButton::Right);		
				break;
			case WM_RBUTTONDOWN:	
				window->OnMouseDown(LOWORD(lParam), HIWORD(lParam), IMR::UI::MouseButton::Right);		
				break;
			case WM_MOUSEMOVE:
				{
					IMR::UI::MouseButton button = IMR::UI::MouseButton::None;
					if ((wParam & MK_LBUTTON) != 0)
						button = IMR::UI::MouseButton::Left;
					else if ((wParam & MK_MBUTTON) != 0)
						button = IMR::UI::MouseButton::Middle;
					else if ((wParam & MK_RBUTTON) != 0)
						button = IMR::UI::MouseButton::Right;
					window->OnMouseMove(LOWORD(lParam), HIWORD(lParam), button);
				}
				break;
			case WM_KEYUP:		
				{
					const KeyMappings::MapType& mappings = KeyMappings::GetDynamicMap();
					KeyMappings::MapType::const_iterator iter = mappings.find(wParam);
					if (iter == mappings.end())
						break;
					window->OnKeyUp(iter->second);	
				}
				break;		
			case WM_KEYDOWN:	
				{
					const KeyMappings::MapType& mappings = KeyMappings::GetDynamicMap();
					KeyMappings::MapType::const_iterator iter = mappings.find(wParam);
					if (iter == mappings.end())
						break;
					window->OnKeyDown(iter->second);	
				}
				break;		
			default:			return DefWindowProc(hWnd, msg, wParam, lParam);
		}

		return 0;
	}


}

#endif
