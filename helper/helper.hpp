#ifndef __HELPER_HELPER_HPP__
#define __HELPER_HELPER_HPP__


#include <iostream>

#include "helper/RenderWindow.hpp"



namespace helper
{


	template < typename RenderWindowType >
	int SampleRendererWindowLifeCycle()
	{
		try
		{
			RenderWindowType window;
			window.Show();
			window.MessageProcessingCycle();

			return 0;
		}
		catch (const std::exception& ex)
		{ 
#ifdef _WIN32
			MessageBoxA(NULL, sc::diagnostic_information(ex), "Error!", MB_ICONSTOP | MB_OK); 
#else
			std::cerr << sc::diagnostic_information(ex) << std::endl;
#endif
		}
		catch (...)
		{
#ifdef _WIN32
			MessageBoxA(NULL, "wtf! O_o", "Error!", MB_ICONSTOP | MB_OK); 
#else
			std::cerr << "wtf! O_o" << std::endl;
#endif
		}

		return 1;
	}


}


#endif
