#pragma once

#include <memory>

#include <sc/math.hpp>
#include <sc/Vector3D.hpp>
#include <IMR/GFX/Renderer.hpp>



namespace helper
{


	inline IMR::GFX::IRenderable* CreateCube(const IMR::GFX::Renderer& renderer, float s)
	{
		using namespace IMR::GFX;
		using namespace sc::Math;

		typedef VertexNormalDiffuseTex1<2> VND;

		std::auto_ptr<VertexArray<VND> > va(renderer.CreateVertexArray<VND>(DrawMode::Triangles));
		std::auto_ptr<IndexArray<unsigned short> > ia(renderer.CreateIndexArray<unsigned short>());

		VND vertices[] = 
		{ 
			VND(Vector3D_f(-s, -s, -s),  Vector3D_f(0,  0, -1), TexCoords<2>(0, 1)), 
			VND(Vector3D_f(-s,  s, -s),  Vector3D_f(0,  0, -1), TexCoords<2>(0, 0)), 
			VND(Vector3D_f( s,  s, -s),  Vector3D_f(0,  0, -1), TexCoords<2>(1, 0)), 
			VND(Vector3D_f( s, -s, -s),  Vector3D_f(0,  0, -1), TexCoords<2>(1, 1)), 

			VND(Vector3D_f(-s, -s,  s),  Vector3D_f(0,  0,  1), TexCoords<2>(1, 1)), 
			VND(Vector3D_f(-s,  s,  s),  Vector3D_f(0,  0,  1), TexCoords<2>(1, 0)), 
			VND(Vector3D_f( s,  s,  s),  Vector3D_f(0,  0,  1), TexCoords<2>(0, 0)), 
			VND(Vector3D_f( s, -s,  s),  Vector3D_f(0,  0,  1), TexCoords<2>(0, 1)), 

			VND(Vector3D_f(-s, -s, -s),  Vector3D_f(0, -1,  0), TexCoords<2>(0, 1)), 
			VND(Vector3D_f(-s, -s,  s),  Vector3D_f(0, -1,  0), TexCoords<2>(0, 0)), 
			VND(Vector3D_f( s, -s,  s),  Vector3D_f(0, -1,  0), TexCoords<2>(1, 0)), 
			VND(Vector3D_f( s, -s, -s),  Vector3D_f(0, -1,  0), TexCoords<2>(1, 1)), 

			VND(Vector3D_f(-s,  s, -s),  Vector3D_f(0,  1,  0), TexCoords<2>(0, 1)), 
			VND(Vector3D_f(-s,  s,  s),  Vector3D_f(0,  1,  0), TexCoords<2>(0, 0)), 
			VND(Vector3D_f( s,  s,  s),  Vector3D_f(0,  1,  0), TexCoords<2>(1, 0)), 
			VND(Vector3D_f( s,  s, -s),  Vector3D_f(0,  1,  0), TexCoords<2>(1, 1)), 

			VND(Vector3D_f(-s, -s, -s),	 Vector3D_f(-1, 0,  0), TexCoords<2>(1, 1)), 
			VND(Vector3D_f(-s,  s, -s),  Vector3D_f(-1,  0, 0), TexCoords<2>(1, 0)), 
			VND(Vector3D_f(-s,  s,  s),  Vector3D_f(-1,  0, 0), TexCoords<2>(0, 0)), 
			VND(Vector3D_f(-s, -s,  s),  Vector3D_f(-1,  0, 0), TexCoords<2>(0, 1)), 

			VND(Vector3D_f( s, -s, -s),  Vector3D_f(1,  0,  0), TexCoords<2>(0, 1)), 
			VND(Vector3D_f( s,  s, -s),  Vector3D_f(1,  0,  0), TexCoords<2>(0, 0)), 
			VND(Vector3D_f( s,  s,  s),  Vector3D_f(1,  0,  0), TexCoords<2>(1, 0)), 
			VND(Vector3D_f( s, -s,  s),  Vector3D_f(1,  0,  0), TexCoords<2>(1, 1))
		};

		unsigned short indices[] = 
		{ 
			 0,  1,  2,		 0,  2,  3, 
			 4,  6,  5,		 4,  7,  6, 
			 8, 10,  9,		 8, 11, 10, 
			12, 13, 14,		12, 14, 15,
			16, 18, 17,		16, 19, 18,
			20, 21, 22,		20, 22, 23
		};

		va->SetData(std::vector<VND>(vertices, vertices + sizeof(vertices) / sizeof(vertices[0])));
		ia->SetData(std::vector<unsigned short>(indices, indices + sizeof(indices) / sizeof(indices[0])));

		return va->GetRenderable(ia);
	}



	inline IMR::GFX::IRenderable* CreateTorus(const	IMR::GFX::Renderer& renderer, 
									   float	torusRadius, 
									   float	tubeRadius, 
									   int		numSides, 
									   int		numRings)
	{
		const float	r1	= torusRadius;
		const float	r2	= tubeRadius;
		const int	d	= numSides + 1;
		const int	q	= numRings + 1;

		using namespace IMR::GFX;
		using namespace sc::Math;

		typedef VertexNormalDiffuseTex2<2, 3> VNDT;
		
		std::auto_ptr<IndexArray<unsigned int> > ia(renderer.CreateIndexArray<unsigned int>());
		std::auto_ptr<VertexArray<VNDT> > va(renderer.CreateVertexArray<VNDT>(DrawMode::Triangles));

		std::vector<VNDT> vertices(q * d);
		std::vector<unsigned int> indices(q * d * 6);

		for (int i = 0; i < q; i++)
		{
			float a = (2*PI_f * i) / (q - 1); 

			Vector3D_f old_pos((r1 + r2 * Sin((2*PI_f * -1) / (d - 1))) * Cos(a),	
							   (r1 + r2 * Sin((2*PI_f * -1) / (d - 1))) * Sin(a),	
							   r2 * Cos((2*PI_f * -1) / (d - 1)));

			for (int j = 0; j < d; j++)
			{
				int index = i + j * q;
				float b = (2*PI_f * j) / (d - 1); 

				Vector3D_f pos = Vector3D_f((r1 + r2 * Sin(b)) * Cos(a),	
											(r1 + r2 * Sin(b)) * Sin(a),	
											r2 * Cos(b));

				vertices[index] = 
					VNDT(
						pos, 
						Vector3D_f(Sin(b) * Cos(a),
								   Sin(b) * Sin(a),
								   Cos(b)),
						TexCoords<2>((float)(j) / (d - 1),
									 (float)(i) / (q - 1)),
						TexCoords<3>((pos - old_pos).Normalize())
					);

				old_pos = pos;

				indices[index*6 + 0] = (i + 0) % q + ((j + 0) % d) * q;
				indices[index*6 + 1] = (i + 0) % q + ((j + 1) % d) * q;
				indices[index*6 + 2] = (i + 1) % q + ((j + 1) % d) * q;
				indices[index*6 + 3] = (i + 0) % q + ((j + 0) % d) * q;
				indices[index*6 + 4] = (i + 1) % q + ((j + 1) % d) * q;
				indices[index*6 + 5] = (i + 1) % q + ((j + 0) % d) * q;
			}
		}

		va->SetData(vertices);
		ia->SetData(indices);

		return va->GetRenderable(ia);
	}


	inline IMR::GFX::IRenderable* CreateSphere(const IMR::GFX::Renderer& renderer, float r, float d = 64, float q = 64)
	{
		using namespace IMR::GFX;
		using namespace sc::Math;

		typedef VertexNormalDiffuseTex2<2, 3>	VertexType;

		std::auto_ptr<IndexArray<unsigned> > ia(renderer.CreateIndexArray<unsigned>());
		std::auto_ptr<VertexArray<VertexType> > va(renderer.CreateVertexArray<VertexType>(DrawMode::Triangles));

		std::vector<VertexType> vertices;
		std::vector<unsigned> indices;

		//////////////////////////////////////////////////////////////////////////
		// Copy-pasted old code
		//////////////////////////////////////////////////////////////////////////

		float _1_div_d = 1 / (float)d, _1_div_q = 1 / (float)q;
		float phi, theta;
		float d_phi = (float)(2 * PI_f / d);
		float d_theta = (float)(PI_f / q);
		Vector3D_f vertex[4], normal[4], tangent[4], tex_coord[4];
		phi = 0;

		//glBegin(GL_QUADS);
		for (float i = 0; i < d; i++) {

			float cos_phi = (float)cos(phi), sin_phi = (float)sin(phi);
			float cos_phi_next = (float)cos(phi + d_phi), sin_phi_next = (float)sin(phi + d_phi);
			float cos_theta, sin_theta, cos_d_theta = (float)cos(d_theta), sin_d_theta = (float)sin(d_theta);
			float cos_theta_next, sin_theta_next;

			theta = 0;
			cos_theta = (float)cos(theta);
			sin_theta = (float)sin(theta);

			for (float j = 0; j < q; j++) {

				cos_theta_next = cos_theta * cos_d_theta - sin_theta * sin_d_theta;
				sin_theta_next = sin_theta * cos_d_theta + cos_theta * sin_d_theta;

				vertex[0].X = (float)(cos_phi * sin_theta * r);
				vertex[0].Y = (float)(sin_phi * sin_theta * r);
				vertex[0].Z = (float)(cos_theta * r);
				vertex[1].X = (float)(cos_phi_next * sin_theta * r);
				vertex[1].Y = (float)(sin_phi_next * sin_theta * r);
				vertex[1].Z = (float)(cos_theta * r);
				vertex[2].X = (float)(cos_phi_next * sin_theta_next * r);
				vertex[2].Y = (float)(sin_phi_next * sin_theta_next * r);
				vertex[2].Z = (float)(cos_theta_next * r);
				vertex[3].X = (float)(cos_phi * sin_theta_next * r);
				vertex[3].Y = (float)(sin_phi * sin_theta_next * r);
				vertex[3].Z = (float)(cos_theta_next * r);

				normal[0].X = (float)(cos_phi * sin_theta);
				normal[0].Y = (float)(sin_phi * sin_theta);
				normal[0].Z = (float)(cos_theta);
				normal[1].X = (float)(cos_phi_next * sin_theta);
				normal[1].Y = (float)(sin_phi_next * sin_theta);
				normal[1].Z = (float)(cos_theta);
				normal[2].X = (float)(cos_phi_next * sin_theta_next);
				normal[2].Y = (float)(sin_phi_next * sin_theta_next);
				normal[2].Z = (float)(cos_theta_next);
				normal[3].X = (float)(cos_phi * sin_theta_next);
				normal[3].Y = (float)(sin_phi * sin_theta_next);
				normal[3].Z = (float)(cos_theta_next);

				tangent[0].X = (float)(-sin_phi);
				tangent[0].Y = (float)(cos_phi);
				tangent[0].Z = (float)(0);
				tangent[1].X = (float)(-sin_phi_next);
				tangent[1].Y = (float)(cos_phi_next);
				tangent[1].Z = (float)(0);
				tangent[2].X = (float)(-sin_phi_next);
				tangent[2].Y = (float)(cos_phi_next);
				tangent[2].Z = (float)(0);
				tangent[3].X = (float)(-sin_phi);
				tangent[3].Y = (float)(cos_phi);
				tangent[3].Z = (float)(0);

				tex_coord[0].X = i * _1_div_d;
				tex_coord[0].Y = j * _1_div_q;
				tex_coord[1].X = (i + 1) * _1_div_d;
				tex_coord[1].Y = j * _1_div_q;
				tex_coord[2].X = (i + 1) * _1_div_d;
				tex_coord[2].Y = (j + 1) * _1_div_q;
				tex_coord[3].X = i * _1_div_d;
				tex_coord[3].Y = (j + 1) * _1_div_q;

				for (int k = 0; k < 4; k++) {
					//glNormal3f(normal[k].x, normal[k].y, normal[k].z);
					//glTexCoord2f(tex_coord[k].x, tex_coord[k].y);
					//glMultiTexCoord3f(GL_TEXTURE7_ARB, tangent[k].x, tangent[k].y, tangent[k].z);
					//glVertex3f(vertex[k].x, vertex[k].y, vertex[k].z);
					vertices.push_back(VertexType(vertex[k], normal[k], TexCoords<2>(tex_coord[k].X, tex_coord[k].Y), tangent[k]));
				}
				unsigned ind_first = (unsigned)(vertices.size() - 4);
				indices.push_back(ind_first);
				indices.push_back(ind_first + 1);
				indices.push_back(ind_first + 2);
				indices.push_back(ind_first);
				indices.push_back(ind_first + 2);
				indices.push_back(ind_first + 3);

				cos_theta = cos_theta_next;
				sin_theta = sin_theta_next;
				//			theta += d_theta;
			}
			phi += d_phi;
		}
		//glEnd();

		va->SetData(vertices);
		ia->SetData(indices);

		return va->GetRenderable(ia);
	}



}
