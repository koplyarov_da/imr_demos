#ifdef _WIN32

#pragma once

#include <map>
#include <windows.h>

#include <IMR/GFX/RendererPlaceholder.hpp>
#include <IMR/UI/Controller/MouseControllerAction.hpp>

#include "helper/Key.hpp"
#include "helper/WinAPIMessagePeeker.hpp"

namespace helper
{

	class Window
	{
		SC_NONCOPYABLE(Window);

	private:
		typedef std::map<HWND, Window*> HandleToWindowMap;
		typedef std::pair<HWND, Window*> HandleToWindowPair;


		static HandleToWindowMap	handleToWindowMap_;
		HWND						wndHandle_;
		WNDCLASSW					windowClass_;
		int							cmdShow_;

		static LRESULT CALLBACK WindowFunc(HWND hWnd,UINT msg,WPARAM wParam,LPARAM lParam);

	protected:
		void KillWindow();

	public:
		Window(int width, int height);
		virtual ~Window();

		HWND GetHandle() const { return wndHandle_; }
		HFONT GetFont() const;

		IMR::GFX::RendererPlaceholder GetRendererPlaceholder() const
		{ return GetHandle(); }

		void SetText(const std::string& text);
		void Show();
		void Quit();

		void MessageProcessingCycle()
		{ MessagePeeker::MessageProcessingCycle(); }

		virtual void OnActivate() { }
		virtual void OnDestroy() { }
		virtual void OnPaint() { }
		virtual void OnMouseUp(int x, int y, IMR::UI::MouseButton button) { }
		virtual void OnMouseDown(int x, int y, IMR::UI::MouseButton button) { }
		virtual void OnMouseMove(int x, int y, IMR::UI::MouseButton button) { }
		virtual void OnMouseWheel(int x, int y, int delta) { }
		virtual void OnKeyUp(Key) { }
		virtual void OnKeyDown(Key) { }
	};


}


#endif
