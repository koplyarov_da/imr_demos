#ifndef __RENDERWINDOW_HPP___
#define __RENDERWINDOW_HPP___


#include <stdlib.h>

#include <sc/shared_ptr.hpp>
#include <sc/StringUtils.hpp>

#include <IMR/GFX/Renderer.hpp>
#include <IMR/GFX/Auxiliary/FPSCounter.hpp>

#include "helper/Window.hpp"


namespace helper
{


	template < typename Renderer >
	class RenderWindow : public Window
	{
	private:
		sc::shared_ptr<Renderer>							_renderer;
		sc::shared_ptr<IMR::GFX::Auxiliary::FPSCounter>		_fpsCounter;

	public:
		RenderWindow() 
			: Window(800, 600)
		{ _renderer = new Renderer((Window&)*this); }

		~RenderWindow()
		{ _renderer.reset(); }

		virtual void OnPaint() 
		{
			if (!_fpsCounter)
				_fpsCounter.reset(new IMR::GFX::Auxiliary::FPSCounter);
			SetText("IMR::GFX demo (" + _renderer->template GetRendererInfo<IMR::GFX::RendererInfo::RendererType>().ToString() + ", fps = " + sc::ToString(_fpsCounter->Get()) + ")");
			_renderer->Render(); 
			_fpsCounter->NextFrame();
		}
		
		virtual void OnKeyDown(Key key) 
		{ if (key == Key::Escape) OnDestroy(); }
		
		virtual void OnDestroy() 
		{ Quit(); }

	protected:
		Renderer& GetRenderer()
		{ return *_renderer; }
	};


}


#endif
