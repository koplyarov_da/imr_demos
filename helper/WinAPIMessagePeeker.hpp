#ifdef _WIN32

#pragma once


#include <windows.h>



namespace helper
{


	/*static*/ struct MessagePeeker
	{
		static void MessageProcessingCycle()
		{
			MSG msg;

			for (;;) 
			{
				while (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) ) 
				{
					if (GetMessage(&msg, NULL, 0, 0)) 
					{ 
						TranslateMessage(&msg);
						DispatchMessage(&msg);
					}
					else return;
				}
				//Sleep(1);
			} 
		}
	};


}


#endif