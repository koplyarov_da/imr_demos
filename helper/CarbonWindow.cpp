#if defined(_DARWIN)

#ifndef __HELPER_CARBONWINDO_H__
#define __HELPER_CARBONWINDO_H__

#include "helper/CarbonWindow.hpp"

#include <sc/CT/ValToValMap.hpp>
#include <sc/exceptions.hpp>

#include "helper/CarbonWindow.hpp"



namespace helper
{

	typedef sc::CT::ValToValMap_1
		<
			int, Key::Values, 
			SC_CT_PAIR( 0, Key::Escape )
		> KeyMappings;

	Window *g_window = 0;

	Window::Window(int width, int height)
	{
		Rect content_bounds;

		CFStringRef window_title = CFSTR("Carbon Mini Application");
		WindowClass window_class = kDocumentWindowClass;
		WindowAttributes attributes =
			kWindowStandardDocumentAttributes |
			kWindowStandardHandlerAttribute |
			kWindowLiveResizeAttribute;

		SetRect(&content_bounds, 100, 100, 900, 700);

		CreateNewWindow (window_class,
						attributes,
						&content_bounds,
						&_wnd);

		SetWindowTitleWithCFString (_wnd, window_title);
		CFRelease(window_title);

		EventTypeSpec event_list[] = {
			{ kEventClassWindow,	kEventWindowClose }, 
			{ kEventClassWindow,	kEventWindowBoundsChanged }, 
			{ kEventClassMouse,		kEventMouseDown }, 
			{ kEventClassMouse,		kEventMouseUp }, 
			{ kEventClassMouse,		kEventMouseMoved }, 
			{ kEventClassMouse,		kEventMouseDragged }
		};   

		InstallWindowEventHandler(_wnd,
								NewEventHandlerUPP (MyWindowEventHandler),
								GetEventTypeCount(event_list),
								event_list, _wnd, NULL);
		
		_renderTimerUPP = NewEventLoopTimerUPP(RenderTimedEventProcessor);
		OSStatus err = InstallEventLoopTimer(GetCurrentEventLoop(),
									0,
									kEventDurationMillisecond,
									_renderTimerUPP,
									NULL, // User Data
									&_renderTimer);
																					
		g_window = this;
	}


	Window::~Window()
	{
		g_window = 0;
	}


	void Window::SetText(const std::string& text)
	{ }


	void Window::Show()
	{
		ShowWindow(_wnd);
	}


	void Quit()
	{
		exit(0)
	}


	int Window::GetFont() const
	{
		//SC_THROW(sc::not_implemented_exception());
		
		return 0;
	}

	void Window::RenderTimedEventProcessor(EventLoopTimerRef inTimer, void* timeData)
	{
		if (g_window)
			g_window->OnPaint();
	}

	OSStatus Window::MyWindowEventHandler (EventHandlerCallRef myHandler, EventRef theEvent, void* userData)
	{
		struct WindowPortSetter
		{
			WindowPortSetter(WindowRef windowRef)
			{
				GetPort(&_oldPort);
				SetPortWindowPort(windowRef);
			}

			~WindowPortSetter()
			{ SetPort(_oldPort); }

		private:
			GrafPtr		_oldPort;
		};


		struct Helper
		{
			static IMR::UI::MouseButton GetMouseButton(EventRef theEvent)
			{
				EventMouseButton carbon_button;
				GetEventParameter(theEvent, kEventParamMouseButton, typeMouseButton, NULL, sizeof(carbon_button), NULL, &carbon_button);
				switch (carbon_button)
				{
				case kEventMouseButtonPrimary:
					return IMR::UI::MouseButton::Left;
				case kEventMouseButtonSecondary:
					return IMR::UI::MouseButton::Right;
				case kEventMouseButtonTertiary:
					return IMR::UI::MouseButton::Middle;
				}
				return IMR::UI::MouseButton::None;
			}

			static Point GetLocation(EventRef theEvent, WindowRef wnd)
			{
				Point location;
				GetEventParameter(theEvent, kEventParamMouseLocation, typeQDPoint, NULL, sizeof(location), NULL, &location);
				WindowPortSetter wnd_port_setter(wnd);
				GlobalToLocal(&location);
				return location;
			}
		};

		//TODO: obtain window object and call the event handlers

		OSStatus result = eventNotHandledErr;

		switch (GetEventKind (theEvent))
		{
			case kEventWindowClose:
				QuitApplicationEventLoop();
				result = noErr;
				break;

			case kEventWindowBoundsChanged:
				result = noErr;
				break;

			case kEventMouseDown:
				{
					Point location = Helper::GetLocation(theEvent, g_window->_wnd);
					if (location.v < 0)
						break;
					IMR::UI::MouseButton button = Helper::GetMouseButton(theEvent);
					g_window->OnMouseDown(location.h, location.v, button);
				}
				break;

			case kEventMouseUp:
				{
					Point location = Helper::GetLocation(theEvent, g_window->_wnd);
					if (location.v < 0)
						break;
					IMR::UI::MouseButton button = Helper::GetMouseButton(theEvent);
					g_window->OnMouseUp(location.h, location.v, button);
				}
				break;

			case kEventMouseMoved:
				{
					Point location = Helper::GetLocation(theEvent, g_window->_wnd);
					if (location.v < 0)
						break;
					g_window->OnMouseMove(location.h, location.v, IMR::UI::MouseButton::None);
				}
				break;

			case kEventMouseDragged:
				{
					Point location = Helper::GetLocation(theEvent, g_window->_wnd);
					if (location.v < 0)
						break;
					IMR::UI::MouseButton button = Helper::GetMouseButton(theEvent);
					g_window->OnMouseMove(location.h, location.v, button);
				}
				break;
		}

		return result;
	}


}

#endif

#endif
