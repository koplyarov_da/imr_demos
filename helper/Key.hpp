#ifndef __HELPER_KEYS_HPP__
#define __HELPER_KEYS_HPP__


#include <sc/EnumClass.hpp>
#include <sc/sc.hpp>


namespace helper
{

	struct Key
	{
		SC_ENUM_VALUES
		(
			Escape
		);

		SC_ENUM_CLASS(Key);
	};

}


#endif
