#if !defined(_WIN32) && !defined(_DARWIN)

#pragma once

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <IMR/GFX/RendererPlaceholder.hpp>
#include <IMR/UI/Controller/MouseControllerAction.hpp>

#include "helper/Key.hpp"


namespace helper
{


	class Window
	{
	private:
		::Display*	_display;
		::Window	_window;
		bool		_working;

	public:
		Window(int width, int height);
		virtual ~Window();

		::Window GetWindow() const { return _window; }
		Display* GetDisplay() const { return _display; }
		::XFontStruct* GetFont() const;

		IMR::GFX::RendererPlaceholder GetRendererPlaceholder() const
		{ return IMR::GFX::RendererPlaceholder(GetDisplay(), GetWindow()); }

		void SetText(const std::string& text);
		void Show();
		void Quit();

		void MessageProcessingCycle();

		virtual void OnActivate() { }
		virtual void OnDestroy() { }
		virtual void OnPaint() { }
		virtual void OnMouseUp(int x, int y, IMR::UI::MouseButton button) { }
		virtual void OnMouseDown(int x, int y, IMR::UI::MouseButton button) { }
		virtual void OnMouseMove(int x, int y, IMR::UI::MouseButton button) { }
		virtual void OnKeyUp(Key key) { }
		virtual void OnKeyDown(Key key) { }
	};


}


#endif
