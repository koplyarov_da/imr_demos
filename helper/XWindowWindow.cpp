#if !defined(_WIN32) && !defined(_DARWIN)


#include <stdexcept>

#include <sc/CT/ValToValMap.hpp>
#include <sc/exceptions.hpp>

#include "helper/Window.hpp"



namespace helper
{

	typedef sc::CT::ValToValMap_1
		<
			unsigned int, Key::Values, 
			SC_CT_PAIR( 9, Key::Escape )
		> KeyMappings;


	//std::map<HWND, Window*> Window::handleToWindowMap_;


	Window::Window(int width, int height)
		: _working(true)
	{
		XSetWindowAttributes windowAttributes;
		Colormap colorMap;
		int errorBase;
		int eventBase;

		_display = XOpenDisplay( NULL );

		if( _display == NULL )
		{
			SC_THROW(std::runtime_error("Could not open display!"));
		}

		width = DisplayWidth(_display, 0);
		height = DisplayHeight(_display, 0);

		windowAttributes.colormap     = CopyFromParent;
		windowAttributes.border_pixel = 0;
		windowAttributes.event_mask   = ExposureMask           |
										VisibilityChangeMask   |
										KeyPressMask           |
										KeyReleaseMask         |
										ButtonPressMask        |
										ButtonReleaseMask      |
										PointerMotionMask      |
										StructureNotifyMask    |
										SubstructureNotifyMask |
										FocusChangeMask;
		
		_window = XCreateSimpleWindow( _display, 
										RootWindow(_display, DefaultScreen(_display)), 
										0, 0, width, height, 0, 0, 0);
		XSetStandardProperties( _display,
								_window,
								"GLX Sample",
								"GLX Sample",
								0/*None*/,
								NULL,
								0,
								NULL );
		
		XSetWindowAttributes window_attrs;
		
		window_attrs.event_mask =	ExposureMask           |
                                    VisibilityChangeMask   |
                                    KeyPressMask           |
                                    KeyReleaseMask         |
                                    ButtonPressMask        |
                                    ButtonReleaseMask      |
                                    PointerMotionMask      |
                                    StructureNotifyMask    |
                                    SubstructureNotifyMask |
                                    FocusChangeMask;

		if (0 == XChangeWindowAttributes(GetDisplay(), GetWindow(), CWEventMask, &window_attrs))
			SC_THROW(std::runtime_error("XChangeWindowAttributes failed!"));
	}


	Window::~Window()
	{
		XUnmapWindow(_display, _window);
		XDestroyWindow(_display, _window);
		XCloseDisplay(_display);
	}

	::XFontStruct* Window::GetFont() const
	{
		//::Font result = XLoadFont(_display, "fixed");
		//::Font result = XLoadFont(_display, "-*-arial-*-*-*-*-*-*-*-*-*-*-*-u");
		::Font font = XLoadFont(_display, "-*-times-*-*-*-*-14-*-*-*-*-*-iso10646-*");
		::XFontStruct* font_struct = XQueryFont(_display, font);
		return font_struct;
	}


	void Window::SetText(const std::string& text)
	{ ::XStoreName(_display, _window, text.c_str()); }


	void Window::Show()
	{
		XMapWindow(_display, _window);
	}

	
	void Window::Quit()
	{
		_working = false;
	}


	void Window::MessageProcessingCycle()
	{
		XEvent event;

		while(_working)
		{
			while(_working && XPending(_display)) // Loop to compress events
			{
				XNextEvent(_display, &event);

				switch(event.type)
				{
					case KeyPress:
					{
						const KeyMappings::MapType& mappings = KeyMappings::GetDynamicMap();
						KeyMappings::MapType::const_iterator iter = mappings.find(event.xkey.keycode);
						if (iter == mappings.end())
							break;
						OnKeyDown(iter->second);	
						//fprintf( stderr, "KeyPress event\n" );
					}
					break;

					case KeyRelease:
					{
						const KeyMappings::MapType& mappings = KeyMappings::GetDynamicMap();
						KeyMappings::MapType::const_iterator iter = mappings.find(event.xkey.keycode);
						if (iter == mappings.end())
							break;
						OnKeyUp(iter->second);	
						//fprintf( stderr, "KeyRelease event\n" );
					}
					break;

					case ButtonPress:
					{
						IMR::UI::MouseButton button = IMR::UI::MouseButton::None;
						switch (event.xbutton.button)
						{
						case Button1:	button = IMR::UI::MouseButton::Left;	break;
						case Button2:	button = IMR::UI::MouseButton::Middle;	break;
						case Button3:	button = IMR::UI::MouseButton::Right;	break;
						}
						OnMouseDown(event.xbutton.x, event.xbutton.y, button);
					}
					break;

					case ButtonRelease:
					{
						IMR::UI::MouseButton button = IMR::UI::MouseButton::None;
						switch (event.xbutton.button)
						{
						case Button1:	button = IMR::UI::MouseButton::Left;	break;
						case Button2:	button = IMR::UI::MouseButton::Middle;	break;
						case Button3:	button = IMR::UI::MouseButton::Right;	break;
						}
						OnMouseUp(event.xbutton.x, event.xbutton.y, button);
					}
					break;

					case MotionNotify:
					{
						IMR::UI::MouseButton button = IMR::UI::MouseButton::None;
						if ((event.xmotion.state & Button1Mask) != 0)
							button = IMR::UI::MouseButton::Left;
						else if ((event.xmotion.state & Button2Mask) != 0)
							button = IMR::UI::MouseButton::Middle;
						else if ((event.xmotion.state & Button3Mask) != 0)
							button = IMR::UI::MouseButton::Right;
						OnMouseMove(event.xmotion.x, event.xmotion.y, button);
					}
					break;

					case Expose:
					{
						//fprintf( stderr, "Expose event\n" ); // ???
					}
					break;

					case ConfigureNotify:
					{
						//glViewport( 0, 0, event.xconfigure.width, event.xconfigure.height );
					}
					break;
				}
			}

			OnPaint();
			//render();
		}
	}


}

#endif
