#ifndef __HELPER_WINDOW_HPP__
#define __HELPER_WINDOW_HPP__


#if defined(_WIN32)
#	include	<helper/WinAPIWindow.hpp>
#elif defined(_DARWIN)
#	include	<helper/CarbonWindow.hpp>
#else
#	include	<helper/XWindowWindow.hpp>
#endif


#endif 
